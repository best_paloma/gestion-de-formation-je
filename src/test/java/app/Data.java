package app;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.app.enums.AbsenceType;
import com.app.enums.PaiementType;
import com.app.enums.TypeSeance;
import com.app.model.*;
import com.app.service.*;


public class Data {

	private static ClassPathXmlApplicationContext context;

	public static void main(String[] args) {

		context = new ClassPathXmlApplicationContext(
				new String[] { "applicationContext.xml" });
		
		/**
		 * Permissions
		 */
		PermissionService ps = (PermissionService) context.getBean("permissionService");
		
		ps.add(new Permission("CAN_ACCESS_ADMIN", "Acceder à la page d'administration"));
		
		ps.add(new Permission("CAN_ACCESS_PARAMETRAGE", "Effectuer des modifications sur les paramètres de l'application."));
		
		ps.add(new Permission("PERSONNEL_READ", "Consulter la liste du personnel"));
		ps.add(new Permission("PERSONNEL_EDIT", "Ajouter, Modifier et supprimer un personnel"));
		
		ps.add(new Permission("ENSEIGNANT_READ", "Consulter la liste des enseigants"));
		ps.add(new Permission("ENSEIGNANT_EDIT", "Ajouter, Modifier et supprimer un enseignant"));
		
		ps.add(new Permission("ABSENCES_READ", "Consulter la liste des absences"));
		ps.add(new Permission("ABSENCES_EDIT", "Ajouter, Modifier et supprimer une absence"));
		
		ps.add(new Permission("RETARDS_READ", "Consulter la liste des retards"));
		ps.add(new Permission("RETARDS_EDIT", "Ajouter, Modifier et supprimer un retard"));
		
		ps.add(new Permission("ETUDIANT_LISTE_READ", "Consulter la liste des etudiants"));
		ps.add(new Permission("ETUDIANT_LISTE_EDIT", "Ajouter, Modifier et supprimer un etudiant"));
		
		ps.add(new Permission("MATIERE_READ", "Consulter la liste des matieres"));
		ps.add(new Permission("MATIERE_EDIT", "Ajouter, Modifier et supprimer une matiere"));
		
		ps.add(new Permission("MODULE_READ", "Consulter la liste des modules"));
		ps.add(new Permission("MODULE_EDIT", "Ajouter, Modifier et supprimer un module"));
		
		ps.add(new Permission("CONTROLE_READ", "Consulter la liste des controles"));
		ps.add(new Permission("CONTROLE_EDIT", "Ajouter, Modifier et supprimer un controle"));
		
		ps.add(new Permission("ETUDIANT_INSCRIPTION_READ", "Consulter la liste des inscriptions des etudiants"));
		ps.add(new Permission("ETUDIANT_INSCRIPTION_EDIT", "Ajouter, Modifier et supprimer une inscription d'un etudiant"));

		ps.add(new Permission("ETUDIANT_BULLETIN_READ", "Consulter la liste les bulletins de notes"));
		ps.add(new Permission("ETUDIANT_BULLETIN_EDIT", "Ajouter, Modifier et supprimer un bulettin de notes"));
		
		ps.add(new Permission("ETUDIANT_DOCUMENT_READ", "Consulter la liste les documents"));
		ps.add(new Permission("ETUDIANT_DOCUMENT_EDIT", "Ajouter, Modifier et supprimer un document"));
		
		ps.add(new Permission("ETUDIANT_EMPLOIE_READ", "Consulter la liste les emploies de temps"));
		ps.add(new Permission("ETUDIANT_EMPLOIE_EDIT", "Ajouter, Modifier et supprimer un emploie de temps"));
		
		ps.add(new Permission("VERSEMENT_READ", "Consulter la liste les versements"));
		ps.add(new Permission("VERSEMENT_EDIT", "Ajouter, Modifier et supprimer un versement"));
		
		/**
		 * Roles
		 */
		RoleService rs = (RoleService) context.getBean("roleService");
		
		
		List<Permission> permissionsList = ps.getAll();
		
		Role r1 = new Role();
		r1.setName("SUPER_ADMIN");
		r1.setPermissions(permissionsList);
		rs.add(r1);
		
		List<Permission> permissionsList2 = new ArrayList<Permission>();
		permissionsList2.add(ps.findByName("PERSONNEL_EDIT"));
		permissionsList2.add(ps.findByName("PERSONNEL_READ"));
		permissionsList2.add(ps.findByName("CAN_ACCESS_ADMIN"));
		
		
		Role r2 = new Role();
		r2.setName("ROLE_USER");
		r2.setPermissions(permissionsList2);
		rs.add(r2);
		
		List<Permission> permissionsList3 = new ArrayList<Permission>();
		permissionsList3.add(ps.findByName("CAN_ACCESS_ADMIN"));
		
		Role r3 = new Role();
		r3.setName("ROLE_USER2");
		r3.setPermissions(permissionsList3);
		rs.add(r3);
		
		/**
		 * Users
		 */

		UserService us = (UserService) context.getBean("userService");
		User u1 = new User();
		u1.setActivated(true);
		u1.setPassword("e10adc3949ba59abbe56e057f20f883e"); // Mot de passe 123456
		u1.setUsername("admin");
		u1.setDate_creation(new DateTime());
		List<Role> roles = new ArrayList<Role>();
		roles.add(rs.findByName("SUPER_ADMIN"));
		u1.setRoles(roles);
		us.add(u1);
		
		
		/**
		 * Peut accéder a enseignant mais pas personnel
		 */
		User u2 = new User();
		u2.setActivated(true);
		u2.setPassword("e10adc3949ba59abbe56e057f20f883e"); // Mot de passe 123456
		u2.setUsername("hamza");
		u2.setDate_creation(new DateTime());
		List<Role> roles2 = new ArrayList<Role>();
		roles2.add(rs.findByName("ROLE_USER"));
		u2.setRoles(roles2);
		us.add(u2);
		
		/**
		 * Ne peut acceder ni a enseigant ni a personnel
		 */
		User u3 = new User();
		u3.setActivated(true);
		u3.setPassword("e10adc3949ba59abbe56e057f20f883e"); // Mot de passe 123456
		u3.setUsername("john");
		u3.setDate_creation(new DateTime());
		List<Role> roles3 = new ArrayList<Role>();
		roles3.add(rs.findByName("ROLE_USER2"));
		u3.setRoles(roles3);
		us.add(u3);
		
		
		/**
		 * Ajout de ville
		 */
		VilleService villeService = (VilleService) context.getBean("villeService");
		Ville v1 = new Ville();
		v1.setIntitule("EL JADIDA");
		villeService.add(v1);
		Ville v2 = new Ville();
		v2.setIntitule("RABAT");
		villeService.add(v2);
		
		
		/**
		 * Ajout de centre
		 */
		CentreService centreService = (CentreService) context.getBean("centreService");
		Centre c1 = new Centre();
		c1.setIntitule("IGA EL JADIDA");
		c1.setVille(villeService.findById(1));
		centreService.add(c1);
		
		Centre c2 = new Centre();
		c2.setIntitule("IGA RABAT");
		c2.setVille(villeService.findById(2));
		centreService.add(c2);
		
		
		/**
		 * Ajout Année univairsitaire
		 */
		AnneeService anneeService = (AnneeService) context.getBean("anneeService");
		Annee an1 = new Annee();
		an1.setActived(false);
		an1.setIntitule("2013/2014");
		anneeService.add(an1);
		
		Annee an2 = new Annee();
		an2.setActived(true);
		an2.setIntitule("2014/2015");
		anneeService.add(an2);
		
		/**
		 * Ajout formation
		 */
		FormationService formationService = (FormationService) context.getBean("formationService");
		Formation f1 = new Formation();
		f1.setIntitule("Formation Initial");
		f1.setAnnee(anneeService.findById(2));
		f1.setCentre(centreService.findById(1));
		formationService.add(f1);
		
		Formation f2 = new Formation();
		f2.setIntitule("Formation Continue");
		f2.setAnnee(anneeService.findById(2));
		f2.setCentre(centreService.findById(1));
		formationService.add(f2);

		
		/**
		 * Ajout Filiere
		 */
		FiliereService filiereService = (FiliereService) context.getBean("filiereService");
		Filiere fl1 = new Filiere();
		fl1.setIntitule("Ingenerie");
		fl1.setCentre(centreService.findById(1));
		fl1.setAnnee(anneeService.findById(2));
		fl1.setFormation(formationService.findById(1));
		filiereService.add(fl1);
		
		Filiere fl2 = new Filiere();
		fl2.setIntitule("Management");
		fl2.setCentre(centreService.findById(1));
		fl2.setAnnee(anneeService.findById(2));
		fl2.setFormation(formationService.findById(1));
		filiereService.add(fl2);
		
		
		/**
		 * Ajout niveaux
		 */
		NiveauxService niveauxService = (NiveauxService) context.getBean("niveauxService");
		Niveaux nv1 = new Niveaux();
		nv1.setIntitule("1ER EI");
		nv1.setAnnee(anneeService.findById(2));
		nv1.setFormation(formationService.findById(1));
		nv1.setCentre(centreService.findById(1));
		nv1.setFiliere(filiereService.findById(1));
		niveauxService.add(nv1);
		
		Niveaux nv3 = new Niveaux();
		nv3.setIntitule("2Em EI");
		nv3.setAnnee(anneeService.findById(2));
		nv3.setFormation(formationService.findById(1));
		nv3.setCentre(centreService.findById(1));
		nv3.setFiliere(filiereService.findById(1));
		niveauxService.add(nv3);
		
		Niveaux nv2 = new Niveaux();
		nv2.setIntitule("1ER MANAGEMENT");
		nv2.setAnnee(anneeService.findById(2));
		nv2.setFormation(formationService.findById(1));
		nv2.setCentre(centreService.findById(1));
		nv2.setFiliere(filiereService.findById(2));
		niveauxService.add(nv2);
		
		Niveaux nv4 = new Niveaux();
		nv4.setIntitule("2Em MANAGEMENT");
		nv4.setAnnee(anneeService.findById(2));
		nv4.setFormation(formationService.findById(1));
		nv4.setCentre(centreService.findById(1));
		nv4.setFiliere(filiereService.findById(2));
		niveauxService.add(nv4);
		
		/**
		 * Ajout Salles
		 */
		SalleService salleService = (SalleService) context.getBean("SalleService");
		Salle s1 = new Salle();
		s1.setIntitule("E1");
		s1.setCentre(centreService.findById(1));
		s1.setAnnee(anneeService.findById(2));
		salleService.add(s1);
		Salle s2 = new Salle();
		s2.setIntitule("E2");
		s2.setCentre(centreService.findById(1));
		s2.setAnnee(anneeService.findById(2));
		salleService.add(s2);
		Salle s3 = new Salle();
		s3.setIntitule("A1");
		s3.setCentre(centreService.findById(1));
		s3.setAnnee(anneeService.findById(2));
		salleService.add(s3);
		Salle s4 = new Salle();
		s4.setIntitule("A2");
		s4.setCentre(centreService.findById(1));
		s4.setAnnee(anneeService.findById(2));
		salleService.add(s4);
		Salle s5 = new Salle();
		s5.setIntitule("B1");
		s5.setCentre(centreService.findById(1));
		s5.setAnnee(anneeService.findById(2));
		salleService.add(s5);
		Salle s6 = new Salle();
		s6.setIntitule("B2");
		s6.setCentre(centreService.findById(1));
		s6.setAnnee(anneeService.findById(2));
		salleService.add(s6);
		
		/**
		 * Ajout Semestre
		 */
		SemestreService semestreService = (SemestreService) context.getBean("semestreService");
		Semestre ss1 = new Semestre();
		ss1.setIntitule("S1");
		ss1.setAnnee(anneeService.findById(2));
		ss1.setCentre(centreService.findById(1));
		ss1.setFormation(formationService.findById(1));
		ss1.setCentre(centreService.findById(1));
		ss1.setFiliere(filiereService.findById(2));
		ss1.setNiveaux(niveauxService.findById(1));
		semestreService.add(ss1);
		
		Semestre ss2 = new Semestre();
		ss2.setIntitule("S2");
		ss2.setAnnee(anneeService.findById(2));
		ss2.setCentre(centreService.findById(1));
		ss2.setFormation(formationService.findById(1));
		ss2.setCentre(centreService.findById(1));
		ss2.setFiliere(filiereService.findById(2));
		ss2.setNiveaux(niveauxService.findById(1));
		semestreService.add(ss2);
		
		Semestre ss3 = new Semestre();
		ss3.setIntitule("S3");
		ss3.setAnnee(anneeService.findById(2));
		ss3.setCentre(centreService.findById(1));
		ss3.setFormation(formationService.findById(1));
		ss3.setCentre(centreService.findById(1));
		ss3.setFiliere(filiereService.findById(2));
		ss3.setNiveaux(niveauxService.findById(1));
		semestreService.add(ss3);
		
		
		
		/**
		 * Ajout Module
		 */
		ModuleService moduleService = (ModuleService) context.getBean("ModuleService");
		Module md1 = new Module();
		md1.setIntitule("Culture et communication 1");
		md1.setContextuel("CC1");
		md1.setAnnee(anneeService.findById(2));
		md1.setCentre(centreService.findById(1));
		md1.setFiliere(filiereService.findById(2));
		md1.setNiveau(niveauxService.findById(1));
		md1.setFormation(formationService.findById(1));
		md1.setFiliere(filiereService.findById(2));
		md1.setCoefficient(2);
		md1.setSemestre(semestreService.findById(1));
		moduleService.add(md1);
		
		Module md2 = new Module();
		md2.setIntitule("Base de donnees avances");
		md2.setContextuel("BDDA");
		md2.setAnnee(anneeService.findById(2));
		md2.setCentre(centreService.findById(1));
		md2.setFiliere(filiereService.findById(2));
		md2.setNiveau(niveauxService.findById(1));
		md2.setFormation(formationService.findById(1));
		md2.setFiliere(filiereService.findById(2));
		md2.setCoefficient(2);
		md2.setSemestre(semestreService.findById(1));
		moduleService.add(md2);
		
		Module md3 = new Module();
		md3.setIntitule("Algorithmique et techniques de programmation 1");
		md3.setAnnee(anneeService.findById(2));
		md3.setContextuel("ATP1");
		md3.setCentre(centreService.findById(1));
		md3.setFiliere(filiereService.findById(2));
		md3.setNiveau(niveauxService.findById(1));
		md3.setFormation(formationService.findById(1));
		md3.setFiliere(filiereService.findById(2));
		md3.setCoefficient(2);
		md3.setSemestre(semestreService.findById(1));
		moduleService.add(md3);
		
		Module md4 = new Module();
		md4.setIntitule("Algorithmique et techniques de programmation 2");
		md4.setContextuel("ATP2");
		md4.setAnnee(anneeService.findById(2));
		md4.setCentre(centreService.findById(1));
		md4.setFiliere(filiereService.findById(2));
		md4.setNiveau(niveauxService.findById(1));
		md4.setFormation(formationService.findById(1));
		md4.setFiliere(filiereService.findById(2));
		md4.setCoefficient(2);
		md4.setSemestre(semestreService.findById(1));
		moduleService.add(md4);
		
		Module md5 = new Module();
		md5.setIntitule("Programmation oriente objet");
		md5.setContextuel("POO");
		md5.setAnnee(anneeService.findById(2));
		md5.setCentre(centreService.findById(1));
		md5.setFiliere(filiereService.findById(2));
		md5.setNiveau(niveauxService.findById(1));
		md5.setFormation(formationService.findById(1));
		md5.setFiliere(filiereService.findById(2));
		md5.setCoefficient(2);
		md5.setSemestre(semestreService.findById(1));
		moduleService.add(md5);
		
		Module md6 = new Module();
		md6.setIntitule("Systemes et reseaux informatiques");
		md6.setContextuel("SRI");
		md6.setAnnee(anneeService.findById(2));
		md6.setCentre(centreService.findById(1));
		md6.setFiliere(filiereService.findById(2));
		md6.setNiveau(niveauxService.findById(1));
		md6.setFormation(formationService.findById(1));
		md6.setFiliere(filiereService.findById(2));
		md6.setCoefficient(2);
		md6.setSemestre(semestreService.findById(1));
		moduleService.add(md6);
		
		Module md7 = new Module();
		md7.setIntitule("Developpement Web");
		md7.setAnnee(anneeService.findById(2));
		md7.setContextuel("DW1");
		md7.setCentre(centreService.findById(1));
		md7.setFiliere(filiereService.findById(2));
		md7.setNiveau(niveauxService.findById(1));
		md7.setFormation(formationService.findById(1));
		md7.setFiliere(filiereService.findById(2));
		md7.setCoefficient(2);
		md7.setSemestre(semestreService.findById(1));
		moduleService.add(md7);
		
		/**
		 * Ajout Enseigants
		 */
		EnseignantService enseignantService = (EnseignantService) context.getBean("enseignantService");
		Enseignant en1 = new Enseignant();
		en1.setNom("Sabri");
		en1.setPrenom("Yassine");
		en1.setCin("MXXXXX");
		en1.setDate_entree(new DateTime());
		en1.setBanque("BMCE");
		en1.setDiplome("Doctorat");
		en1.setEmail("sabri.yassine@gmail.com");
		en1.setTelephone("06XXXXXXX");
		enseignantService.add(en1);
		
		Enseignant en2 = new Enseignant();
		en2.setNom("Bousmah");
		en2.setPrenom("Mohammed");
		en2.setCin("MXXXXX");
		en2.setDate_entree(new DateTime());
		en2.setBanque("BMCE");
		en2.setDiplome("Doctorat");
		en2.setEmail("cccc@gmail.com");
		en2.setTelephone("06XXXXXXX");
		enseignantService.add(en2);
		
		Enseignant en3 = new Enseignant();
		en3.setNom("Lakrami");
		en3.setPrenom("Fatima");
		en3.setCin("MXXXXX");
		en3.setDate_entree(new DateTime());
		en3.setBanque("BMCE");
		en3.setDiplome("Doctorat");
		en3.setEmail("cccc@gmail.com");
		en3.setTelephone("06XXXXXXX");
		enseignantService.add(en3);
		
		
		/**
		 * Ajout Matiere
		 *
		*/
		MatiereService matiereService = (MatiereService) context.getBean("MatiereService");
		Matiere mt1 = new Matiere();
		mt1.setIntitule("Anglais technique 1");
		mt1.setAnnee(anneeService.findById(2));
		mt1.setCentre(centreService.findById(1));
		mt1.setFiliere(filiereService.findById(2));
		mt1.setFormation(formationService.findById(1));
		mt1.setFiliere(filiereService.findById(2));
		mt1.setSemestre(semestreService.findById(1));
		mt1.setModule(moduleService.findById(1));
		mt1.setCoeff(1);
		
		mt1.setNiveaux(niveauxService.findById(1));
		matiereService.add(mt1);
		
		
		Matiere mt2 = new Matiere();
		mt2.setIntitule("Developpement Web 1");
		mt2.setAnnee(anneeService.findById(2));
		mt2.setCentre(centreService.findById(1));
		mt2.setFiliere(filiereService.findById(2));
		mt2.setFormation(formationService.findById(1));
		mt2.setFiliere(filiereService.findById(2));
		mt2.setSemestre(semestreService.findById(1));
		mt2.setModule(moduleService.findById(7));
		mt2.setCoeff(1);
		mt2.setEnseignant(enseignantService.findById(1));
		mt2.setNiveaux(niveauxService.findById(1));
		matiereService.add(mt2);
		
		Matiere mt3 = new Matiere();
		mt3.setIntitule("Bases de donnees orientees objet");
		mt3.setAnnee(anneeService.findById(2));
		mt3.setCentre(centreService.findById(1));
		mt3.setFiliere(filiereService.findById(2));
		mt3.setFormation(formationService.findById(1));
		mt3.setFiliere(filiereService.findById(2));
		mt3.setSemestre(semestreService.findById(1));
		mt3.setModule(moduleService.findById(2));
		mt3.setCoeff(1);
		mt3.setNiveaux(niveauxService.findById(1));
		matiereService.add(mt3);
		
		Matiere mt4 = new Matiere();
		mt4.setIntitule("Programmation JAVA Avancee");
		mt4.setAnnee(anneeService.findById(2));
		mt4.setCentre(centreService.findById(1));
		mt4.setFiliere(filiereService.findById(2));
		mt4.setFormation(formationService.findById(1));
		mt4.setFiliere(filiereService.findById(2));
		mt4.setSemestre(semestreService.findById(1));
		mt4.setModule(moduleService.findById(5));
		mt4.setCoeff(1);
		mt4.setNiveaux(niveauxService.findById(1));
		matiereService.add(mt4);
		
		
		/**
		 * Ajout Etudiant
		 */
		EtudiantService etudiantService = (EtudiantService) context.getBean("etudiantService");
		Etudiant et1 = new Etudiant();
		et1.setPrenom("Hamza");
		et1.setNom("Bahlaouane");
		et1.setCIN("M66543");
		et1.setDate(new DateTime());
		et1.setEmail("bahlaouane.hamza1@gmail.com");
		et1.setLieu("EL JADIDA");
		et1.setTelephone("0615162087");
		et1.setAnnee(anneeService.findById(2));
		et1.setCentre(centreService.findById(1));
		et1.setFormation(formationService.findById(1));
		et1.setCentre(centreService.findById(1));
		et1.setFiliere(filiereService.findById(2));
		et1.setNiveaux(niveauxService.findById(1));
		et1.setType(PaiementType.annuel);
		//et1.setUtilisateur(us.findById(2));
		//et1.setCentre(centreService.findById(1));
		etudiantService.add(et1);
		
		Etudiant et2 = new Etudiant();
		et2.setPrenom("Mehdi");
		et2.setNom("Frago");
		et2.setCIN("MXXXX");
		et2.setDate(new DateTime());
		et2.setEmail("mehdi.frago@gmail.com");
		et2.setLieu("EL JADIDA");
		et2.setTelephone("06XXXXXX");
		et2.setAnnee(anneeService.findById(2));
		et2.setCentre(centreService.findById(1));
		et2.setFormation(formationService.findById(1));
		et2.setCentre(centreService.findById(1));
		et2.setFiliere(filiereService.findById(2));
		et2.setNiveaux(niveauxService.findById(1));
		et2.setType(PaiementType.mentuel);
		//et2.setUtilisateur(us.findById(2));
		//et2.setCentre(centreService.findById(1));
		etudiantService.add(et2);
		
		Etudiant et3 = new Etudiant();
		et3.setPrenom("Souhail");
		et3.setNom("Harrati");
		et3.setCIN("MXXXX");
		et3.setDate(new DateTime());
		et3.setEmail("souhail.harrati@gmail.com");
		et3.setLieu("EL JADIDA");
		et3.setTelephone("06XXXXXX");
		et3.setAnnee(anneeService.findById(2));
		et3.setCentre(centreService.findById(1));
		et3.setFormation(formationService.findById(1));
		et3.setCentre(centreService.findById(1));
		et3.setFiliere(filiereService.findById(2));
		et3.setNiveaux(niveauxService.findById(1));
		et3.setType(PaiementType.trimestriel);
		//et3.setUtilisateur(us.findById(2));
		//et3.setCentre(centreService.findById(1));
		etudiantService.add(et3);
		
		Etudiant et4 = new Etudiant();
		et4.setPrenom("Hicham");
		et4.setNom("Fatih");
		et4.setCIN("MXXXX");
		et4.setDate(new DateTime());
		et4.setEmail("fatih.hicham@gmail.com");
		et4.setLieu("EL JADIDA");
		et4.setTelephone("06XXXXXX");
		et4.setAnnee(anneeService.findById(2));
		et4.setCentre(centreService.findById(1));
		et4.setFormation(formationService.findById(1));
		et4.setCentre(centreService.findById(1));
		et4.setFiliere(filiereService.findById(2));
		et4.setNiveaux(niveauxService.findById(1));
		et4.setType(PaiementType.annuel);
		//et4.setUtilisateur(us.findById(2));
		//et4.setCentre(centreService.findById(1));
		etudiantService.add(et4);
		
		
		/**
		 * Ajout Absence
		 */
		AbsenceService absenceService = (AbsenceService) context.getBean("absenceService");
		Absence abs1 = new Absence();
		abs1.setAnnee(anneeService.findById(2));
		abs1.setCentre(centreService.findById(1));
		abs1.setFiliere(filiereService.findById(2));
		abs1.setFormation(formationService.findById(1));
		abs1.setFiliere(filiereService.findById(2));
		abs1.setSemestre(semestreService.findById(1));
		abs1.setMatiere(matiereService.findById(1));
		abs1.setModule(moduleService.findById(1));
		abs1.setEtudiant(etudiantService.findById(1));
		abs1.setNiveau(niveauxService.findById(1));
		abs1.setDateSeance(new DateTime());
		abs1.setHeureDebut("08h30");
		abs1.setHeureFin("10h00");
		abs1.setJustifier(false);
		abs1.setJustification("");
		abs1.setTypeSeance(TypeSeance.TD);
		abs1.setType(AbsenceType.Absence);
		absenceService.add(abs1);
		
		Absence abs2 = new Absence();
		abs2.setAnnee(anneeService.findById(2));
		abs2.setCentre(centreService.findById(1));
		abs2.setFiliere(filiereService.findById(2));
		abs2.setFormation(formationService.findById(1));
		abs2.setFiliere(filiereService.findById(2));
		abs2.setSemestre(semestreService.findById(1));
		abs2.setMatiere(matiereService.findById(1));
		abs2.setModule(moduleService.findById(1));
		abs2.setEtudiant(etudiantService.findById(1));
		abs2.setNiveau(niveauxService.findById(1));
		abs2.setDateSeance(new DateTime());
		abs2.setHeureDebut("10h15");
		abs2.setHeureFin("11h45");
		abs2.setJustifier(false);
		abs2.setJustification("");
		abs2.setTypeSeance(TypeSeance.TP);
		abs2.setType(AbsenceType.Absence);
		absenceService.add(abs2);
		
		
		Absence abs3 = new Absence();
		abs3.setAnnee(anneeService.findById(2));
		abs3.setCentre(centreService.findById(1));
		abs3.setFiliere(filiereService.findById(2));
		abs3.setFormation(formationService.findById(1));
		abs3.setFiliere(filiereService.findById(2));
		abs3.setSemestre(semestreService.findById(1));
		abs3.setMatiere(matiereService.findById(2));
		abs3.setModule(moduleService.findById(1));
		abs3.setEtudiant(etudiantService.findById(2));
		abs3.setNiveau(niveauxService.findById(1));
		abs3.setDateSeance(new DateTime());
		abs3.setHeureDebut("10h15");
		abs3.setHeureFin("11h45");
		abs3.setJustifier(true);
		abs3.setJustification("BDE kif dima");
		abs3.setTypeSeance(TypeSeance.Cours);
		abs3.setType(AbsenceType.Absence);
		absenceService.add(abs3);
		
		
		/*
		 * Messages
		 */
		MessageService messageService = (MessageService) context.getBean("messageService");
		MessageThread msg1 = new MessageThread();
		msg1.setSubject("Hello World!");
		msg1.setDateMessage(new DateTime());
		msg1.setFrom(us.findById(1));
		msg1.setTo(us.findById(2));
		Message cmsg1 = new Message();
		cmsg1.setRead(2); // read
		cmsg1.setFrom(us.findById(1));
		cmsg1.setTo(us.findById(2));
		cmsg1.setDateMessage(new DateTime());
		cmsg1.setMessageThread(msg1);
		cmsg1.setContenu("Salut");
		
		Message cmsg2 = new Message();
		cmsg2.setFrom(us.findById(2));
		cmsg2.setTo(us.findById(1));
		cmsg2.setDateMessage(new DateTime());
		cmsg2.setMessageThread(msg1);
		cmsg2.setContenu("Salut");
		cmsg2.setRead(2); // not read yet
		msg1.addMessage(cmsg1);
		msg1.addMessage(cmsg2);
		msg1.setRead(false);
		messageService.add(msg1);
		
		MessageThread msg2 = new MessageThread();
		msg2.setSubject("Hello World!");
		msg2.setDateMessage(new DateTime());
		msg2.setFrom(us.findById(2));
		msg2.setTo(us.findById(1));
		msg2.setRead(false);
		messageService.add(msg2);
		
		
		MessageThread msg3 = new MessageThread();
		msg3.setSubject("Hello World!");
		msg3.setDateMessage(new DateTime());
		msg3.setFrom(us.findById(1));
		msg3.setTo(us.findById(3));
		msg3.setRead(false);
		messageService.add(msg3);
		
		
	}

}
