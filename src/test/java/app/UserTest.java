package app;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.app.model.Role;
import com.app.service.RoleService;

public class UserTest {
	@Before
	public void Setup() throws Exception {

	}

	@Test
	public void test() {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				new String[] { "applicationContext.xml" });
		RoleService rs = (RoleService) context.getBean("roleService");
		List<Role> list1 = rs.getAll();
		Role r1 = new Role();
		r1.setName("ADMIN");
		rs.add(r1);
		Role r2 = new Role();
		r2.setName("USER");
		rs.add(r2);

		List<Role> list2 = rs.getAll();
		assertTrue((list1.size() + 2) == list2.size());

	}

}
