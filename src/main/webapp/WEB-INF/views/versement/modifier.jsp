<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>


<%! String menuActuel = "menu_versement";  %>


<jsp:include page="../../views/layout/header.jsp" />
<jsp:include page="../../views/layout/leftpanel.jsp" />
<jsp:include page="../../views/layout/topmenu.jsp" />

<div class="pageheader">
      <h2><i class="fa fa-eur"></i> Versement <span>Modifier versement </span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="versement" />">Versement</a></li>
          <li class="active">Modifier versement</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title">Modification  du versement N° : ${versementForm.id}</h3>
        </div>
        <f:form method="post" action="update" modelAttribute="versementForm">
        <f:hidden path="id" />
			
        <div class="panel-body">
        <c:if test="${success != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success}
							</div>
						</c:if>
						
						<c:if test="${error != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error}
							</div>
						</c:if>
			


				<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">


						  <div class="form-group">
                    <label class="control-label">Date Versement</label>
                    <f:input path="dateSeance" cssClass="form-control datepicker-multiplee"/>
                    <f:errors path="dateSeance" cssClass="errors"></f:errors>

                  </div>
						
        	 
                  				
		<div class="form-group">
			 <label class="control-label">Montant</label>
			 <div class="input-group">
			 
                  <f:input path="montant" type="text" class="form-control" />
                  <span class="input-group-addon">DHs</span>
                </div>
			<f:errors path="montant" cssClass="error"></f:errors>
               </div>
               

                  <div class="form-group">
                    <label class="control-label">Etudiant</label>
                    <f:select path="etudiant" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN ETUDIANT--</f:option>
					    <f:options items="${etudiants}" itemValue="id" itemLabel="nom" />
					</f:select>
                    <f:errors path="etudiant" cssClass="error"></f:errors>
                  </div>
                  
                  
                    <div class="form-group">
                    <label class="control-label">Semestre</label>
                    <f:select path="semestre" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN SEMESTRE--</f:option>
					    <f:options items="${semestres}" itemValue="id" itemLabel="intitule" />
					</f:select>
                    <f:errors path="semestre" cssClass="error"></f:errors>
                  </div>
                   <div class="form-group">
                    <label class="control-label">Type Versement</label>
                    <c:forEach items="${types_versement}" var="value">
     					<div class="radio"><label><f:radiobutton path="type" value="${value}" /> ${value}</label></div>
					</c:forEach>
					<f:errors path="type" cssClass="error"></f:errors>
                  </div>
               </div>
               
               <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
                <div class="form-group">
                    <label class="control-label">Niveau</label>
                    <f:select path="niveau" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN NIVEAU--</f:option>
					    <f:options items="${niveaux}" itemValue="id" itemLabel="intitule" />
					</f:select>
                    <f:errors path="niveau" cssClass="error"></f:errors>
                  </div>
               
              
                  
                  
                  
                  <div class="form-group">
                    <label class="control-label">Annee</label>
                    <f:select path="annee" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE ANNEE--</f:option>
					    <f:options items="${annees}" itemValue="id" itemLabel="intitule" />
					</f:select>
                    <f:errors path="annee" cssClass="error"></f:errors>
                  </div>
                  
                  
                  <div class="form-group">
                    <label class="control-label">Centre</label>
                    <f:select path="centre" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN CENTRE--</f:option>
					    <f:options items="${centres}" itemValue="id" itemLabel="intitule" />
					</f:select>
                    <f:errors path="centre" cssClass="error"></f:errors>
                  </div>
                  
                  
                  
                    <div class="form-group">
                    <label class="control-label">Formation</label>
                    <f:select path="formation" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE FORMATION--</f:option>
					    <f:options items="${formations}" itemValue="id" itemLabel="intitule" />
					</f:select>
                    <f:errors path="formation" cssClass="error"></f:errors>
                  </div>
                  
                  
                    <div class="form-group">
                    <label class="control-label">Filiere</label>
                    <f:select path="filiere" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE FILIERE--</f:option>
					    <f:options items="${filieres}" itemValue="id" itemLabel="intitule" />
					</f:select>
                    <f:errors path="filiere" cssClass="error"></f:errors>
                  </div>
                  
                  
                  
                  
                  
               </div>
               </div>
               </div>
          
         <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-12">
				  <button type="submit" class="btn btn-primary">Valider</button>&nbsp;
				  <button type="reset" class="btn btn-default">Annuler</button>
				  <a href="delete?id=${versementForm.id}" class="btn btn-danger delete">SUPPRIMER</a>
				  
				</div>
			 </div>
		  </div><!-- panel-footer -->
		  </f:form>
     </div>
     </div>
 
<jsp:include page="../../views/layout/rightpanel.jsp" />
<jsp:include page="../../views/layout/footer.jsp">
	<jsp:param name="javascripts" value="/assets/js/chosen.jquery.min.js" />
</jsp:include>


<script>
  jQuery(document).ready(function() {
	    jQuery("a#<%= menuActuel %>").parent("li").addClass("active");

    
    jQuery('.datepicker-multiplee').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
      });
    
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
 

  });
</script>

</body>
</html>