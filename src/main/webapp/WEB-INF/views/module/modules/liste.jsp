<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<%! String menuActuel = "menu_module";  %>
<%! String sousMenuActuel = "menu_module_modules";  %>

<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-users"></i> Module <span>Modules</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="modules" />">Module</a></li>
          <li class="active">Modules</li>
        </ol>
      </div>
    </div>
    
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title"><a href="<c:url value="/module/modules/ajouter" />" class="btn btn-primary-alt"><i class="fa fa-plus"></i> Ajouter</a> </h3>
        </div>
        <div class="panel-body">
        <c:if test="${success_module_delete != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success_module_delete}
							</div>
						</c:if>
						
						<c:if test="${error_module_delete != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error_module_delete}
							</div>
						</c:if>
						
        	<div class="table-responsive">
	            <table class="table" id="table1">
	              <thead>
	                 <tr>
	                     
                                <th>Intitulé</th>
                                <th>Contextuel</th>
                                <th>Coefficient</th>
                                <th>Annee</th>
                                <th>Formation</th>
                                <th>Filiere</th>
                                <th>Niveau</th>
                               
	                 </tr>
	              </thead>
	              <tbody>
	             <c:forEach var="v" items="${liste_modules}">
								<tr>
                                    
                                    <td><a href="modules/editModule?id=${v.id}">${v.intitule}</a></td>
                                    <td>${v.contextuel}</td>
                                    <td>${v.coefficient}</td>
                                 
                                    <td>${v.annee.intitule}</td>
                                    <td>${v.formation.intitule}</td>
                                    <td>${v.filiere.intitule}</td>
                                     <td>${v.niveau.intitule}</td>
                                 
                                </tr>
								</c:forEach>
								
	                 
	              </tbody>
	             </table>
             </div>
        </div>
     </div>
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp" />

<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>

<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    
    jQuery('#table1').dataTable({
        "sPaginationType": "full_numbers"
      });
    
 // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });

  });
</script>

</body>
</html>