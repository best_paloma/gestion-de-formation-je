<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>

<jsp:include page="layout/header.jsp" />
<jsp:include page="layout/leftpanel.jsp" />
<jsp:include page="layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-info"></i> GoFormation <span>Aide</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="#.">GoFormation</a></li>
          <li class="active">Aide</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
    
    <div class="panel-group panel-group-dark" id="accordion2">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2">
                    C'est quoi GoFormation
                  </a>
                </h4>
              </div>
              <div id="collapseOne2" class="panel-collapse collapse in">
                <div class="panel-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" class="collapsed" data-parent="#accordion2" href="#collapseTwo2">
                    Comment Faire ca?
                  </a>
                </h4>
              </div>
              <div id="collapseTwo2" class="panel-collapse collapse">
                <div class="panel-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" class="collapsed" data-parent="#accordion2" href="#collapseThree2">
                    What about this ?
                  </a>
                </h4>
              </div>
              <div id="collapseThree2" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.</p>
                  <p>Food truck quinoai nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
                </div>
              </div>
            </div>
          </div>
          
     
 	</div>
<jsp:include page="layout/rightpanel.jsp" />
<jsp:include page="layout/footer.jsp" />


</body>
</html>