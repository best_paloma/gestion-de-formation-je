<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>

<%! String menuActuel = "menu_notes";  %>
<%! String sousMenuActuel = "menu_notes_modules";  %>


<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-users"></i> Notes <span>Modules</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="notes" />">Notes</a></li>
          <li class="active">Modules</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
     <div class="panel panel-default">
       
        
        <div class="panel-body">
        <c:if test="${success_module != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success_module}
							</div>
						</c:if>
						
						<c:if test="${error_module != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error_module}
							</div>
						</c:if>
			
						<form action="">
             <div class="row">
             <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                 <div class="form-group">
                    <label class=" col-sm-3 control-label">Module </label>
        	   <select name="Module" id="intitule" >
               <c:forEach var="result" items="${liste_modules}">
               <option value="${id}"><c:out value="${result.intitule}" /></option>
               </c:forEach>
               </select>
               </div>
               <div class="form-group">
                    <label class=" col-sm-3 control-label">Centre </label>
        	   <select name="Centre" id="intitule"  >
               <c:forEach var="result" items="${liste_centre}">
               <option value="${id}"><c:out value="${result.intitule}" /></option>
               </c:forEach>
               </select>
               </div>
                  <div class="form-group">
                    <label class=" col-sm-3 control-label">Annee </label>
        	   <select name="Annee" id="intitule"  >
               <c:forEach var="result" items="${liste_annee}">
               <option value="${id}"><c:out value="${result.intitule}" /></option>
               </c:forEach>
               </select>
               </div>  
                 <div class="form-group">
                    <label class=" col-sm-3 control-label">Formation </label>
        	   <select name="Formation" id="intitule"  >
               <c:forEach var="result" items="${liste_formation}">
               <option value="${id}"><c:out value="${result.intitule}" /></option>
               </c:forEach>
               </select>
               </div>  
                   </div>
                   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                
                <div class="form-group">
                    <label class=" col-sm-3 control-label">Filiere </label>
        	   <select name="Filiere" id="intitule"  >
               <c:forEach var="result" items="${liste_filiere}">
               <option value="${id}"><c:out value="${result.intitule}" /></option>
               </c:forEach>
               </select>
               </div>
                     <div class="form-group">
                    <label class=" col-sm-3 control-label">Niveau </label>
        	   <select name="Niveau" id="intitule"  >
               <c:forEach var="result" items="${liste_niveaux}">
               <option value="${id}"><c:out value="${result.intitule}" /></option>
               </c:forEach>
               </select>
               </div>
                   <div class="form-group">
                    <label class=" col-sm-3 control-label">Semestre</label>
        	   <select name="Semestre" id="intitule"  >
               <c:forEach var="result" items="${semestre}">
               <option value="${id}"><c:out value="${result.intitule}" /></option>
               </c:forEach>
               </select>
               </div>
                 
                         </div>
                     </div>
                	<br>	
                    <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-12">
                                <button type="submit" class="btn btn-primary">Valider</button>
                            </div>
                        </div>
                     
                    </form>
                    <div class="table-responsive">
                       <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Etudiant</th>
                                <th>Note</th>
                                
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            <tr>
                            		<td colspan="3">Effectuer une recherche pour consulter le resultat</td>
                            	</tr>
								
                            </tbody>
                        </table>
                        </div>

     </div></div>
     </div>
    
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp">
	<jsp:param name="javascripts" value="/assets/js/chosen.jquery.min.js" />
</jsp:include>


<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    jQuery('.datepicker-multiplee').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
      });
    
    jQuery("select").chosen({'width':'100%','white-space':'nowrap'});
 

  });
</script>

</body>
</html>