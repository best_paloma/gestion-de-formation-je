<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>


<%! String menuActuel = "menu_parametrage";  %>
<%! String sousMenuActuel = "menu_parametrage_utilisateur";  %>

<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-users"></i> Paramétrage <span>Utilisateurs</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="#.">Paramétrage</a></li>
          <li class="active">Utilisateurs</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title">Modification "${userForm.username}"</h3>
        </div>
        <f:form method="post" action="update" modelAttribute="userForm">
        <f:hidden path="id" />
			
        <div class="panel-body">
        <c:if test="${success_user != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success_user}
							</div>
						</c:if>
						
						<c:if test="${error_user != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error_user}
							</div>
						</c:if>
			
						
        	 <div class="row mb15">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nom d'utilisateur <span class="asterisk">*</span></label>
                    <f:input path="username" cssClass="form-control" size="20"/>
                    <f:errors path="username" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-6 -->
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Mot de passe <span class="asterisk">*</span></label>
                    <f:password path="password" cssClass="form-control" size="20"/>
                    <f:errors path="password" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-6 -->
              </div><!-- row -->
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">État</label>
                    <div class="radio"><label><f:radiobutton path="activated" value="true" />Activé </label></div>
                    <div class="radio"><label><f:radiobutton path="activated" value="false"/>Désactivé</label></div>
                  </div>
                </div><!-- col-sm-6 -->
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Profils</label>
                    <c:forEach var="item" items="${listeRoles}">
	                    <c:set var="checked" value="false" />
						<c:forEach var="row" items="${userForm.roles}">
						  <c:if test="${row.id eq item.id}">
						    <c:set var="checked" value="true" />
						  </c:if>
						</c:forEach>
						<c:choose>
					      <c:when test="${checked==true}">
					           <div class="checkbox block"><label><f:checkbox path="roles" checked="checked"  value="${item.id}" /> ${item.name}</label></div>
					      </c:when>
					      <c:otherwise>
					          <div class="checkbox block"><label><f:checkbox path="roles" value="${item.id}" /> ${item.name}</label></div>
					      </c:otherwise>
						</c:choose>
					</c:forEach>
				<f:errors path="roles" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-6 -->
              </div><!-- row -->
              
              
             
        </div>
         <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-12">
				  <button type="submit" class="btn btn-primary">Valider</button>&nbsp;
				  <button type="reset" class="btn btn-default">Annuler</button> &nbsp;
				  <a href="delete?id=${userForm.id}" class="btn btn-danger delete">SUPPRIMER</a>
				</div>
			 </div>
		  </div><!-- panel-footer -->
		  </f:form>
     </div>
 </div>
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp">
	<jsp:param name="javascripts" value="/assets/js/chosen.jquery.min.js" />
</jsp:include>


<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    jQuery('.datepicker-multiplee').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
      });
    
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
    
    jQuery("a.delete").click(function(e){
    	  e.preventDefault();
    	  var url = $(this).attr("href");
        swal(
        {
            title: "Êtes-vous sure?",
            text: "Vous ne serez pas en mesure de récupérer cet élément",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui, supprimez-le!",
            cancelButtonText: "Non, annuler!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm)
        {
            if (isConfirm) {
            	window.location = url;
                swal("Suppression!", "L'élement va être supprimé dans quelques instants.", "success");
            }
            else {
                swal("Annulé", "Aucune opération n'a été effectuer", "error");
            }
        });
        
        return false;
      });
 

  });
</script>

</body>
</html>