<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<%! String menuActuel = "menu_parametrage";  %>
<%! String sousMenuActuel = "menu_parametrage_utilisateur";  %>

<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-users"></i> Paramétrage <span>Utilisateurs</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="#.">Paramétrage</a></li>
          <li class="active">Utilisateurs</li>
        </ol>
      </div>
    </div>
    
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title"><a href="<c:url value="/parametrage/utilisateurs/ajouter" />" class="btn btn-primary-alt"><i class="fa fa-plus"></i> Ajouter</a> </h3>
        </div>
        <div class="panel-body">
        <c:if test="${success_user_delete != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success_user_delete}
							</div>
						</c:if>
						
						
						<c:if test="${error_user_delete != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error_user_delete}
							</div>
						</c:if>
						
        	<div class="table-responsive">
	            <table class="table" id="table1">
	              <thead>
	                 <tr>
	                    <th>Nom d'utilisateur</th>
	                    <th>Date création</th>
	                    <th>Dernière connexion</th>
	                    <th>Role(s)</th>
	                    <th>État</th>
	                 </tr>
	              </thead>
	              <tbody>
	              <c:forEach var="v" items="${liste}">
								<tr>
         
                                    <td><a href="utilisateurs/edit?id=${v.id}">${v.username}</a></td>
                                    <td><joda:format pattern="MM/dd/yyyy HH:mm:ss" value="${v.date_creation}" /></td>
                                    <td><joda:format pattern="MM/dd/yyyy HH:mm:ss" value="${v.lastLogin}" /></td>
                                    <td>
                                    	<c:forEach var="row" items="${v.roles}" varStatus="loop">
										  <span class="label label-info">${row.name}</span><c:if test="${!loop.last}">, </c:if>
										</c:forEach>
                                    </td>
                                    <td>
                                    <c:choose>
									      <c:when test="${v.activated==true}">
									      	<span class="label label-success">Activé</span>
									      </c:when>
									      <c:otherwise>
									      	<span class="label label-danger">Désactivé</span>
									      </c:otherwise>
									</c:choose>
                                    </td>
                                </tr>
					</c:forEach>
								
	                 
	              </tbody>
	             </table>
             </div>
        </div>
     </div>
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp" />

<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>

<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    
    jQuery('#table1').dataTable({
        "sPaginationType": "full_numbers",
        "aoColumnDefs" : [ {
    	    "bSortable" : false,
    	    "aTargets" : [ "no-sort" ]
    	} ]
      });
    
 // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });

  });
</script>

</body>
</html>