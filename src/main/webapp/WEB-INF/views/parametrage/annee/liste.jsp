<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>


<%! String menuActuel = "menu_parametrage";  %>
<%! String sousMenuActuel = "menu_parametrage_annee";  %>


<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-cogs"></i> Paramétrage <span>Années</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="/parametrage/annee" />">Paramétrage</a></li>
          <li class="active">Années</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="#" class="panel-close">&times;</a>
                    <a href="#" class="minimize">&minus;</a>
                </div><!-- panel-btns -->
          <h3 class="panel-title"><a href="<c:url value="/parametrage/annee/ajouter" />" class="btn btn-primary-alt"><i class="fa fa-plus"></i> Ajouter</a> </h3>
            </div>
            <div class="panel-body">
            
            <c:if test="${success_annee_delete != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success_annee_delete}
							</div>
						</c:if>
						
						<c:if test="${error_annee_delete != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error_annee_delete}
							</div>
						</c:if>
                    

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    
                    
                    <div class="table-responsive">
                       <table class="table table-hidaction table-bordered mb30">
                            <thead>
                            <tr>
                                <th>Intitulé</th>
                                 <th>Actived</th>
                                <th></th>
                            </tr>
                            
                           
                            </thead>
                            <tbody>
                            
                            <c:forEach var="v" items="${liste_annees}">
								<tr>
                                    <td>${v.intitule}</td>
                                     <td>
                                     <c:choose>
									      <c:when test="${v.actived==true}">
									      	<span class="label label-success">Activé</span>
									      </c:when>
									      <c:otherwise>
									      	<span class="label label-danger">Désactivé</span>
									      </c:otherwise>
									</c:choose>
									</td>
                                    <td class="table-action-hide">
                                        <a href="annee/edit?id=${v.id}"><i class="fa fa-pencil"></i></a>
                                        <a href="annee/delete?id=${v.id}" data-id="${v.id}" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
								</c:forEach>
								
                            </tbody>
                        </table>
                    </div><!-- table-responsive -->
                </div><!-- col-md-6 -->

                

            </div><!-- row -->
            </div>
            </div> <!-- /PANEL -->

       
    </div>
    
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp">
	<jsp:param name="javascripts" value="/assets/js/jquery.mousewheel.js" />
	<jsp:param name="javascripts" value="/assets/js/chosen.jquery.min.js" />
	<jsp:param name="javascripts" value="/assets/js/jquery.validate.min.js" />
</jsp:include>


<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");

    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });

    // Chosen Select
  jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});


  // Basic Form
  /*
  jQuery(".configForm").validate({
    highlight: function(element) {
      jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      jQuery(element).closest('.form-group').removeClass('has-error');
    }
  });*/

  jQuery("a.delete-row").click(function(e){
	  e.preventDefault();
	  var url = $(this).attr("href");
    swal(
    {
        title: "Êtes-vous sure?",
        text: "Vous ne serez pas en mesure de récupérer cet élément",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui, supprimez-le!",
        cancelButtonText: "Non, annuler!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm)
    {
        if (isConfirm) {
        	window.location = url;
            //swal("Supprimé!", "L'élement a été supprimé.", "success");
        }
        else {
            swal("Annulé", "Aucune opération n'a été effectuer", "error");
        }
    });
    
    return false;
  });


  });
</script>

</body>
</html>