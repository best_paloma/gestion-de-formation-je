<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>


<%! String menuActuel = "menu_etudiants";  %>
<%! String sousMenuActuel = "menu_etudiants_etudiant";  %>


<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-graduation-cap"></i> Etudiants <span>Etudiant</span><span>Ajouter des etudiants</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="etudiant" />">Etudiants</a></li>
          <li class="active">Etudiants</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title">Ajouter</h3>
        </div>
        <f:form method="post" action="save" modelAttribute="etudiantForm">
			
        <div class="panel-body">
        <c:if test="${success != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success}
							</div>
						</c:if>
						
						<c:if test="${error != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error}
							</div>
						</c:if>
			
			<div class="row ">
             <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label class="control-label">Prénom <span class="asterisk">*</span></label>
                    <f:input path="prenom" cssClass="form-control" size="20"/>
                    <f:errors path="prenom" cssClass="error"></f:errors>
                  </div>
  
                  <div class="form-group">
                    <label class="control-label">Nom <span class="asterisk">*</span></label>
                    <f:input path="nom" cssClass="form-control" size="20"/>
                    <f:errors path="nom" cssClass="error"></f:errors>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label">Compte Utilisateur</label>
                    <f:select path="utilisateur" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN UTILISATEUR--</f:option>
					    <f:options items="${utilisateurs}" itemValue="id" itemLabel="username" />
					</f:select>
                    <f:errors path="utilisateur" cssClass="error"></f:errors>
                  </div>
                  
                  
                   <div class="form-group">
                    <label class="control-label">Date </label>
                    <f:input path="date" cssClass="form-control datepicker-multiplee"/>
                    <f:errors path="date" cssClass="errors"></f:errors>

                  </div>
                  
                  
                  <div class="form-group">
                    <label class="control-label">CIN</label>
                    <f:input path="CIN" cssClass="form-control" size="20"/>
                    <f:errors path="CIN" cssClass="errors"></f:errors>
                    </div>
                    
                
                    
                    
                  <div class="form-group">
                    <label class="control-label">Téléphone</label>
                    <f:input path="telephone" id="phone" cssClass="form-control" size="20"/>
                    <f:errors path="telephone" cssClass="errors"></f:errors>
                  </div>
              
                   <div class="form-group">
                    <label class="control-label">Type Paiement</label>
                    <c:forEach items="${types_paiement}" var="value">
     					<div class="radio"><label><f:radiobutton path="type" value="${value}" /> ${value}</label></div>
					</c:forEach>
					<f:errors path="type" cssClass="error"></f:errors>
                  </div>
                  
                      </div><!-- col-sm-6 -->
             
              
              
              
             <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">
                    <label class="control-label">Lieu de naissance</label>
                    <f:input path="lieu" cssClass="form-control" size="20"/>
                    <f:errors path="lieu" cssClass="errors"></f:errors>
                  </div>
                  
                  
                  
                  
                   <div class="form-group">
                    <label class="control-label">Email</label>
                    <f:input path="email" cssClass="form-control" size="20"/>
                    <f:errors path="email" cssClass="errors"></f:errors>
                  </div>
                  
                  
                  
                  
                       <div class="form-group">
                    <label class="control-label">Annee </label>
                    <f:select path="annee" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE ANNEE--</f:option>
					    <f:options items="${annees}" itemValue="id" itemLabel="intitule" />
					</f:select>
                    <f:errors path="annee" cssClass="error"></f:errors>
                  </div>
                  
                       <div class="form-group">
                    <label class="control-label">Centre </label>
                    <f:select path="centre" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN CENTRE--</f:option>
					    <f:options items="${centres}" itemValue="id" itemLabel="intitule" />
					</f:select>
                    <f:errors path="centre" cssClass="error"></f:errors>
                  </div>
                  
                  
                  
                  
                  
                       <div class="form-group">
                    <label class="control-label">Formation </label>
                    <f:select path="formation" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE FORMATION--</f:option>
					    <f:options items="${formations}" itemValue="id" itemLabel="intitule" />
					</f:select>
                    <f:errors path="formation" cssClass="error"></f:errors>
                  </div>
                  
                  
                  
                     <div class="form-group">
                    <label class="control-label">Filiere </label>
                    <f:select path="filiere" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE FILIERE--</f:option>
					    <f:options items="${filieres}" itemValue="id" itemLabel="intitule" />
					</f:select>
                    <f:errors path="filiere" cssClass="error"></f:errors>
                  </div>
                  
                  
                  
                     <div class="form-group">
                    <label class="control-label">Niveaux </label>
                    <f:select path="niveaux" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN NIVEAUX--</f:option>
					    <f:options items="${niveaux}" itemValue="id" itemLabel="intitule" />
					</f:select>
                    <f:errors path="niveaux" cssClass="error"></f:errors>
                  </div>
                   <div class="form-group">
                    <label class="control-label">Reduction</label>
                    <f:input path="reduction" id="phone" cssClass="form-control" size="20"/>
                    <f:errors path="reduction" cssClass="errors"></f:errors>
                  </div>

                 
              </div>
              </div>
               
  </div>
           
              
         <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-12">
				  <button type="submit" class="btn btn-primary">Valider</button>&nbsp;
				  <button type="reset" class="btn btn-default">Annuler</button>
				</div>
			 </div>
		  </div><!-- panel-footer -->
		  </f:form>
     </div>
     </div>
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp">
<jsp:param name="javascripts" value="/assets/js/chosen.jquery.min.js" />
</jsp:include>

<script src="<c:url value="/assets/js/jquery.validate.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery.maskedinput.min.js" />"></script>


<script>





jQuery(document).ready(function() {
	
	jQuery("#phone").mask("(+212) 06-99-99-99-99");

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    jQuery('.datepicker-multiplee').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
      });
    
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
 

  });

</script>




</body>
</html>