<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<%! String menuActuel = "menu_etudiants";  %>
<%! String sousMenuActuel = "menu_etudiants_absences";  %>

<jsp:include page="../../../views/layout/header.jsp">
	<jsp:param name="stylesheets" value="/assets/css/jquery.gritter.css" />
</jsp:include>
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-users"></i> Étudiants <span>Absences</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="etudiants" />">Étudiants</a></li>
          <li class="active">Absences</li>
        </ol>
      </div>
    </div>
    
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title"><a href="<c:url value="/etudiants/absences/ajouter" />" class="btn btn-primary-alt"><i class="fa fa-plus"></i> Ajouter</a> </h3>
        </div>
        <div class="panel-body">
        <c:if test="${success_delete != null}">
			<div class="alert alert-success" role="alert">
				<strong>Well done!</strong> ${success_delete}
			</div>
		</c:if>
		
		<c:if test="${error_delete != null}">
			<div class="alert alert-danger" role="alert">
				<strong>Oh snap!</strong> ${error_delete}
			</div>
		</c:if>
		
        	<div class="table-responsive">
	            <table class="table" id="table1">
	              <thead>
	                 <tr>
	                 	<th class="no-sort">&amp;</th>
	                    <th>Nom &amp; Prénom</th>
	                    <th>Type de séance</th>
	                    <th>Matière</th>
	                    <th>Date</th>
	                    <th><i class="fa fa-clock-o"></i> Début</th>
	                    <th><i class="fa fa-clock-o"></i> Fin</th>
	                    <th>Jutifié?</th>
	                    <th class="no-sort">Justification</th>
	                 </tr>
	              </thead>
	              <tbody>
	              <c:forEach var="v" items="${liste}">
								<tr>
         							<td><a href="absences/delete?id=${v.id}" class="column-delete delete" style="  color: #D9534F;"><i class="fa fa-trash-o"></i></a> </td>
                                    <td>${v.etudiant.nom} ${v.etudiant.prenom}</td>
                                    <td>${v.typeSeance}</td>
                                    <td>${v.matiere.intitule}</td>
                                    <td><joda:format pattern="MM/dd/yyyy" value="${v.dateSeance}" /></td>
                                    <td>${v.heureDebut}</td>
                                    <td>${v.heureFin}</td>
                                    <td id="justifier-column-${v.id}">
                                    <c:choose>
									      <c:when test="${v.justifier==true}">
									      	<span class="label label-success justifier" data-id="${v.id}" style="cursor:pointer;">Oui</span>
									      </c:when>
									      <c:otherwise>
									      	<span class="label label-warning"  style="cursor:pointer;" data-toggle="modal" data-target="#justification-modal-${v.id}">Non</span>
									      </c:otherwise>
									</c:choose>
                                    </td>
                                    <td id="justification-column-${v.id}">
                                    <c:choose>
									      <c:when test="${v.justifier==true}">
									      	${v.justification}
									      </c:when>
									      <c:otherwise>
									      	--
									      </c:otherwise>
									</c:choose>
                                    </td>
                                </tr>
					</c:forEach>
								
	                 
	              </tbody>
	             </table>
             </div>
        </div>
     </div>
     
     
<c:forEach var="v" items="${liste}">
<div class="modal fade" id="justification-modal-${v.id}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
        <form action="#." method="post" id="justification-form-${v.id}" data-id="${v.id}">
	        <div class="panel panel-dark panel-alt">
			    <div class="panel-heading">
			        <div class="panel-btns">
			            <a  aria-hidden="true" data-dismiss="modal" class="close">&times;</a>
			        </div><!-- panel-btns -->
			        <h3 class="panel-title">Justification</h3>
			        <p>Veuillez entrer une justification pour cette absence</p>
			    </div>
			    <div class="panel-body">
			         <div class="form-group">
			              <div class="col-sm-12">
			                <textarea class="form-control" name="message" rows="5"></textarea>
			              </div>
			            </div>
			    </div>
			    <div class="panel-footer text-right">
			    	<button type="button" class="btn btn-success submit-justification-form" data-id="${v.id}">Valider</button>
			    	<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			    </div>
			</div>
        </form>
        
    </div>
    
  </div>
</div>
 </c:forEach>
 
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp" />

<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery.gritter.min.js" />"></script>

<script>
  jQuery(document).ready(function($) {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    
    jQuery('#table1').dataTable({
        "sPaginationType": "full_numbers",
        "aoColumnDefs" : [ {
    	    "bSortable" : false,
    	    "aTargets" : [ "no-sort" ]
    	} ]
      });
    
 // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });
 
 	$(".submit-justification-form").click(function(){
 		var id = $(this).data("id");
 		var form = $("#justification-form-"+id);
 		var mmodal = $("#justification-modal-"+id);
 		var message = form.find("textarea[name='message']").val();
 		
 		jQuery.post('<c:url value="/etudiants/absences/justifier" />',{
 			"id" : id,
 			"msg": message
 		}, function(data){
 			$("td#justification-column-"+id).text(message);
 	 		$("td#justifier-column-"+id).html('<span class="label label-success">Oui</span>');
 	 	
 	 		//alert(message);
 	 		mmodal.modal('hide');
 	 		jQuery.gritter.add({
 	 			title: 'Succès',
 	 			text: 'Justification appliqué avec succès',
 	 	      class_name: 'growl-success',
 	 	      image: '<c:url value="/assets/images/screen.png" />',
 	 			sticky: false,
 	 			time: ''
 	 		 });
 		});
 		
 		
 		
 	});
 	
 	$("span.justifier").click(function(){
 		var id = $(this).data("id");
 		
 		jQuery.post('<c:url value="/etudiants/absences/nonjustifier" />',{
 			"id" : id
 			}, function(data){
 			$("td#justification-column-"+id).text("--");
 	 		$("td#justifier-column-"+id).html('<span class="label label-warning">Non</span>');
 	 		
 	 		jQuery.gritter.add({
 	 			title: 'Succès',
 	 			text: 'Opération appliqué avec succès',
 	 	      class_name: 'growl-success',
 	 	      image: '<c:url value="/assets/images/screen.png" />',
 	 			sticky: false,
 	 			time: ''
 	 		 });
 		});
 		
 		
 		
 		
 	});
 	
 	jQuery("a.delete").click(function(e){
 	  	  e.preventDefault();
 	  	  var url = $(this).attr("href");
 	      swal(
 	      {
 	          title: "Êtes-vous sure?",
 	          text: "Vous ne serez pas en mesure de récupérer cet élément",
 	          type: "warning",
 	          showCancelButton: true,
 	          confirmButtonColor: "#DD6B55",
 	          confirmButtonText: "Oui, supprimez-le!",
 	          cancelButtonText: "Non, annuler!",
 	          closeOnConfirm: false,
 	          closeOnCancel: false
 	      },
 	      function(isConfirm)
 	      {
 	          if (isConfirm) {
 	          	window.location = url;
 	          	swal("Suppression!", "L'élement va être supprimé dans quelques instants.", "success");
 	          }
 	          else {
 	              swal("Annulé", "Aucune opération n'a été effectuer", "error");
 	          }
 	      });
 	      
 	      return false;
 	    });
 	 

  });
</script>

</body>
</html>