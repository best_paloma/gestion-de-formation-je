<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>

<%! String menuActuel = "menu_etudiants";  %>
<%! String sousMenuActuel = "menu_etudiants_emploies";  %>

<jsp:include page="../../layout/header.jsp" />
<jsp:include page="../../layout/leftpanel.jsp" />
<jsp:include page="../../layout/topmenu.jsp" />



<div class="pageheader">
      <h2><i class="fa fa-graduation-cap"></i> Étudiants <span>Emploie du temps</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="#.">Étudiants</a></li>
          <li class="active">Emploie du temps</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title">Modification</h3>
        </div>
        
        
                <f:form method="post"  action="update" enctype="multipart/form-data" modelAttribute="emploieForm">
      
        <input type="hidden" name="id" value="${emploie.id}" />
			
			<div class="panel-body">
			
				<c:if test="${success != null}">
					<div class="alert alert-success" role="alert">
						<strong>Well done!</strong> ${success}
					</div>
				</c:if>
		
				<c:if test="${error != null}">
					<div class="alert alert-danger" role="alert">
						<strong>Oh snap!</strong> ${error}
					</div>
				</c:if>
              
               <!-- <div class="form-group">
                  <label class="control-label">Fichier PDF</label>
                  <div class="col-lg-12">                  
                  		<input type="file" name="file" class="form-control" required>
                  		<span class="help-block">Veuilez charger un fichier avec l'une des extensions : ( doc, docx, pdf, jpg, png, jpeg ) </span>
                  </div>
                  </div>
                   -->
                  
                  
               <div class="row">
             <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                
    
                
                <div class="form-group">
                  <label class="control-label">Intitulé <span class="asterisk">*</span> </label>
                     <f:input path="intitule" cssClass="form-control" size="20"/>
                     <f:errors path="intitule" cssClass="error"></f:errors>
                  </div>
                
               
           
                <div class="form-group">
                    <label class="control-label">Annee</label>
                    <f:select path="annee" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE ANNEE--</f:option>
					    <f:options items="${liste_annees}" itemValue="id" itemLabel="intitule" />
					</f:select>
					<f:errors path="annee" cssClass="error"></f:errors>
                  </div>
                  
                  
                  <div class="form-group">
                    <label class="control-label">Niveaux</label>
                    <f:select path="niveau" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN NIVEAUX--</f:option>
					    <f:options items="${liste_niveaux}" itemValue="id" itemLabel="intitule" />
					</f:select>
					<f:errors path="niveau" cssClass="error"></f:errors>
                  </div>
                  
                  
                  
                  </div>
             <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
             
             
                   <div class="form-group">
                    <label class="control-label">Formation</label>
                    <f:select path="formation" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN FORMATION--</f:option>
					    <f:options items="${liste_formations}" itemValue="id" itemLabel="intitule" />
					</f:select>
					<f:errors path="formation" cssClass="error"></f:errors>
                  </div>
             
             
                   <div class="form-group">
                    <label class="control-label">Filiere</label>
                    <f:select path="filiere" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE FILIERE--</f:option>
					    <f:options items="${liste_filieres}" itemValue="id" itemLabel="intitule" />
					</f:select>
					<f:errors path="filiere" cssClass="error"></f:errors>
                  </div>
                
              
                  
                   <div class="form-group">
                    <label class="control-label">Semestre</label>
                    <f:select path="semestre" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN SEMESTRE --</f:option>
					    <f:options items="${liste_semestres}" itemValue="id" itemLabel="intitule" />
					</f:select>
					<f:errors path="semestre" cssClass="error"></f:errors>
                  </div>
                </div>
                </div>
                
               
                
                
              </div><!-- panel-body -->
              <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Valider</button>
            <button type="reset" class="btn btn-default">Annuler</button>
              </div><!-- panel-footer -->
		
        
	
		  </f:form>
     </div>
 </div>
<jsp:include page="../../layout/rightpanel.jsp" />
<jsp:include page="../../layout/footer.jsp" />
<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>


<script>
  jQuery(document).ready(function($) {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    
 // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });
    
    
  });
  
  
</script>


</body>
</html>