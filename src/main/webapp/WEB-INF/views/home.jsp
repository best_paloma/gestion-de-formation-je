<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<%! String menuActuel = "menu_dashboard";  %>


<jsp:include page="layout/header.jsp" />
<jsp:include page="layout/leftpanel.jsp" />
<jsp:include page="layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-home"></i> GoFormation <span>Tableau de bord</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="/" />">GoFormation</a></li>
          <li class="active">Tableau de bord</li>
        </ol>
      </div>
    </div>

<div class="contentpanel">
    
    <div class="row">

        <div class="col-sm-6 col-md-3">
          <div class="panel panel-success panel-stat">
            <div class="panel-heading">

              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="<c:url value="/assets/images/is-user.png" />" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">Étudiants</small>
                    <h1>${total_etudiants}</h1>
                  </div>
                </div><!-- row -->

              </div><!-- stat -->

            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->

        <div class="col-sm-6 col-md-3">
          <div class="panel panel-danger panel-stat">
            <div class="panel-heading">

              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="<c:url value="/assets/images/is-document.png" />" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">% Unique Visitors</small>
                    <h1>54.40%</h1>
                  </div>
                </div><!-- row -->


              </div><!-- stat -->

            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->

        <div class="col-sm-6 col-md-3">
          <div class="panel panel-primary panel-stat">
            <div class="panel-heading">

              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="<c:url value="/assets/images/is-document.png" />" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">Page Views</small>
                    <h1>300k+</h1>
                  </div>
                </div><!-- row -->


              </div><!-- stat -->

            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->

        <div class="col-sm-6 col-md-3">
          <div class="panel panel-dark panel-stat">
            <div class="panel-heading">

              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="<c:url value="/assets/images/is-money.png" />" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">Bénéfices</small>
                    <h1>
                    		${total_versements}
                    </h1>
                  </div>
                </div><!-- row -->

  

              </div><!-- stat -->

            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->
      </div><!-- row -->
      
      <div class="row">
        
        <div class="col-sm-6 col-md-8">
        
        
        <div class="panel panel-default">
            <div class="panel-body">
            <h5 class="subtitle mb5">Niveau d'absences / retards</h5>
            <p class="mb15">Duis autem vel eum iriure dolor in hendrerit in vulputate...</p>
            <div id="area-chart" class="body-chart"></div>
            </div><!-- panel-body -->
          </div><!-- panel -->
        
        
        </div> <!-- .col-sm-6 -->
        <div class="col-sm-6 col-md-4">
        
        <div class="panel panel-default">
            <div class="panel-body">
            <h5 class="subtitle mb5">Most Browser Used</h5>
            <p class="mb15">Duis autem vel eum iriure dolor in hendrerit in vulputate Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum officia repudiandae sunt unde rem deleniti.

</p>
            <div id="donut-chart2" class="ex-donut-chart"></div>
            </div><!-- panel-body -->
          </div><!-- panel -->
        
        
        
        </div> <!-- ./col-sm-6 -->
       </div>
       
       <div class="row">
       <div class="col-sm-6 col-md-4">
       
       <div class="panel panel-dark panel-alt widget-todo">
          <div class="panel-heading">
              <div class="panel-btns">
                <a href="" data-toggle="modal" data-target="#todo-settings" class="tooltips" data-toggle="tooltip" title="Settings"><i class="glyphicon glyphicon-cog"></i></a>
                <a href="" id="addnewtodo" class="tooltips" data-toggle="tooltip" title="Add New"><i class="glyphicon glyphicon-plus"></i></a>
              </div><!-- panel-btns -->
              <h3 class="panel-title">Tâches</h3>
            </div>
            <div class="panel-body nopadding">
              <ul class="todo-list" id="todo-list-jq">
              <li class="todo-form hidden">
                         <div class="row">
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="todo-text" placeholder="Votre tâche" />
                            </div>
                            <div class="col-sm-4">
                                <button class="btn btn-primary btn-sm btn-block" id="add-todo">Ajouter</button>
                            </div>
                            </div>
                         </li>
                
              </ul>
            </div><!-- panel-body -->
          </div><!-- panel -->
          
       
        
          </div>
          
          <div class="col-sm-6 col-md-4">
          <div class="panel panel-default panel-alt widget-messaging">
          <div class="panel-heading">
              <div class="panel-btns">
                <a href="<c:url value="/parametrage/journalisation" />" class="panel-edit"><i class="fa fa-info"></i></a>
              </div><!-- panel-btns -->
              <h3 class="panel-title">Journal d'opérations</h3>
            </div>
            <div class="panel-body">
              <ul>
              <c:forEach var="l" items="${logs_liste}">
              <li>
                  <small class="pull-right"><joda:format pattern="MM/dd/yyyy" value="${l.dateop}" /></small>
                  <h4 class="sender">${l.user}</h4>
                  <small>${l.operation}</small>
                </li>
              </c:forEach>
                
                
              </ul>
            </div><!-- panel-body -->
          </div><!-- panel -->
          </div>
          
          <div class="col-sm-6 col-md-4">
	          <div class="row">
	            <div class="col-xs-6">
	              <div class="panel panel-warning panel-alt widget-today">
	                <div class="panel-heading text-center">
	                  <i class="fa fa-calendar-o"></i>
	                </div>
	                <div class="panel-body text-center">
	                <jsp:useBean id="now" class="java.util.Date"/>    
	                  <h3 class="today"><fmt:formatDate value="${now}" dateStyle="long"/></h3>
	                </div><!-- panel-body -->
	              </div><!-- panel -->
	            </div>
	
	            <div class="col-xs-6">
	              <div class="panel panel-danger panel-alt widget-time">
	                <div class="panel-heading text-center">
	                  <i class="glyphicon glyphicon-time"></i>
	                </div>
	                <div class="panel-body text-center">
	                  <h3 class="today"><fmt:formatDate value="${now}" pattern="HH:mm:ss a z" /></h3>
	                </div><!-- panel-body -->
	              </div><!-- panel -->
	            </div>
	          </div>
	          
	           <div class="panel panel-default widget-weather">
            <div class="panel-body">
              <div class="row">
                <div class="col-xs-6 temp text-center">
                  <h1><b id="weather_temp"></b> <span>&#12444;</span></h1>
                  <h5><i class="fa fa-map-marker"></i> &nbsp; <span id="weather_city"></span></h5>
                </div>
                <div class="col-xs-6 weather text-center">
                  <i class="fa fa-cloud weather-icon"></i>
                  <div class="pull-left"><i class="fa fa-umbrella"></i> 1.0mm</div>
                  <div class="pull-right"><i class="fa fa-flag"></i> <span id="weather_wind"></span></div>
                </div>
              </div>
            </div><!-- panel-body -->
          </div><!-- panel -->
          </div>
          
        </div>
        
      
      
 </div> <!-- ./contentpanel -->
    
    
    <div class="modal fade" id="todo-settings" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
            <h4 class="modal-title">Configuration Tâches</h4>
        </div>
        <div class="modal-body">
        	<form id="configuration-todo">
            <div class="form-group">
              <label class="sr-only" for="tbtask">Nombre de Tâches</label>
              <input type="number" id="nombre_taches" class="form-control" id="tbtask" placeholder="Entrer un nombre">
            </div>
            <button type="submit" class="btn btn-primary btn-block">Valider</button>
          </form>
        </div>
    </div>
  </div>
</div>
    
<jsp:include page="layout/rightpanel.jsp" />
<jsp:include page="layout/footer.jsp" />
<!-- 
<script src="<c:url value="/assets/js/flot/jquery.flot.min.js" />"></script>
<script src="<c:url value="/assets/js/flot/jquery.flot.resize.min.js" />"></script>
<script src="<c:url value="/assets/js/flot/jquery.flot.spline.min.js" />"></script>
 -->
<script src="<c:url value="/assets/js/morris.min.js" />"></script>
<script src="<c:url value="/assets/js/raphael-2.1.0.min.js" />"></script>
<script src="<c:url value="/assets/bower_components/simpleWeather/jquery.simpleWeather.min.js" />"></script>

<script>
  jQuery(document).ready(function() {
    jQuery("a#<%= menuActuel %>").parent("li").addClass("active");
    
    function showTooltip(x, y, contents) {
		jQuery('<div id="tooltip" class="tooltipflot">' + contents + '</div>').css( {
		  position: 'absolute',
		  display: 'none',
		  top: y + 5,
		  left: x + 5
		}).appendTo("body").fadeIn(200);
	}
    
 // Donut Chart
    var m1 = new Morris.Donut({
         element: 'donut-chart2',
         data: [
           {label: "Chrome", value: 30},
           {label: "Firefox", value: 20},
           {label: "Opera", value: 20},
           {label: "Safari", value: 20},
           {label: "Internet Explorer", value: 10}
         ],
         colors: ['#D9534F','#1CAF9A','#428BCA','#5BC0DE','#428BCA']
     });
 
 
 
    var mois = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
 
    var m2 = new Morris.Area({
        // ID of the element in which to draw the chart.
        element: 'area-chart',
        // Chart data records -- each entry in this array corresponds to a point on
        // the chart.
        data: [
            { y: 'jan', a: ${absences_m01}, b: ${retards_m01} },
            { y: 'fév', a: ${absences_m02},  b: ${retards_m02} },
            { y: 'mars', a: ${absences_m03},  b: ${retards_m03} },
            { y: 'avr', a: ${absences_m04},  b: ${retards_m04} },
            { y: 'mai', a: ${absences_m05},  b: ${retards_m05} },
            { y: 'juin', a: ${absences_m06},  b: ${retards_m06} },
            { y: 'juil', a: ${absences_m07},  b: ${retards_m07} },
            { y: 'août', a: ${absences_m08},  b: ${retards_m08} },
            { y: 'sep', a: ${absences_m09},  b: ${retards_m09} },
            { y: 'oct', a: ${absences_m10},  b: ${retards_m10} },
            { y: 'nov', a: ${absences_m11}, b: ${retards_m11} },
            { y: 'déc', a: ${absences_m12}, b: ${retards_m12} }
        ],
        xkey: 'y',
        parseTime: false,
        ykeys: ['a', 'b'],
        labels: ['Absences', 'Retards'],
        lineColors: ['#1CAF9A', '#F0AD4E'],
        lineWidth: '1px',
        fillOpacity: 0.8,
        smooth: false,
        hideHover: false
    });
    
    
    if (localStorage['tasks']) {
        var tasks = JSON.parse(localStorage['tasks']);
    }else {
        var tasks = [];
    }
    
    if (localStorage['nbtasks']) {
        var nbtasks = localStorage['nbtasks'];
    }else {
        var nbtasks = 5;
    }
    
    jQuery("#nombre_taches").val(nbtasks);
    
    jQuery("#configuration-todo").submit(function(){
    	localStorage['nbtasks'] = jQuery("#nombre_taches").val();
    	jQuery("#todo-settings").modal("hide");
    	return false;
    });
    
    function guid() {
    	  function s4() {
    	    return Math.floor((1 + Math.random()) * 0x10000)
    	      .toString(16)
    	      .substring(1);
    	  }
    	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    	    s4() + '-' + s4() + s4() + s4();
    	}
    
    function appendTaskToList(task) {
    	
    	var checked = '';
    	if(task.done)
    		checked = "checked";
    	
        $('#todo-list-jq').append('<li>'+
                '<div class="ckbox ckbox-success">'+
                '<input type="checkbox" value="1" data-key="'+task.key+'" id="'+task.key+'" '+checked+' />'+
                '<label for="'+task.key+'">'+task.text+'</label>'+
            '</div>'+
        '</li>');
    }

    
    for(var i=0;i<tasks.length;i++) {
    	if(i>=nbtasks)
    		break;
        appendTaskToList(tasks[i]);
    }
    
    jQuery("#add-todo").click(function(){
    	var val = jQuery("#todo-text").val();
    	
    	var task = {
    		text:val,
    		done:false,
    		key:guid()
    	};
    	// add the task to the array
        tasks.push(task);

        // save to local storage
        localStorage["tasks"] = JSON.stringify(tasks);
        
        // append the name to the list
        appendTaskToList(task);
  
    	jQuery("#todo-text").val("").focus();
    	
    	updateTodo();
    });
    
  //Add New To-Do
    jQuery('#addnewtodo').click(function(){
        
        var todo = jQuery(this).closest('.panel').find('.todo-list');
        todo.find('.todo-form').removeClass("hidden");
        
        return false;
    });
  
    updateTodo();
  
    function updateTodo(){
    	jQuery("#todo-list-jq input[type='checkbox']").change(function(){
        	//console.log("hello change");
        	var key = $(this).data("key");
        	//console.log(key);
        	var task = $.grep(tasks, function(e){ return e.key == key; })[0];
        	//console.log(task);
        	task.done = $(this).is(':checked');
        	localStorage["tasks"] = JSON.stringify(tasks);
        });
    }
  
  
    $.simpleWeather({
        location: "Rabat",
        woeid: "",
        unit: 'c',
        success: function(weather) {
          
        	//console.log(weather);
        	$("#weather_city").text(weather.city);
        	$("#weather_temp").text(weather.temp);
        	$("#weather_wind").text(weather.wind.speed);
          
          //$("#weather").html(html);
        },
        error: function(error) {
          //$("#weather").html('<p>'+error+'</p>');
        }
      });
    
  });
</script>

</body>
</html>