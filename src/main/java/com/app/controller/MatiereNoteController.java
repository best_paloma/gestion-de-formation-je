package com.app.controller;



import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Filiere;
import com.app.model.Formation;
import com.app.model.Matiere;
import com.app.model.Module;
import com.app.model.Niveaux;
import com.app.model.Semestre;
import com.app.propertyeditors.AnneeEditor;
import com.app.propertyeditors.CentreEditor;
import com.app.propertyeditors.FiliereEditor;
import com.app.propertyeditors.FormationEditor;
import com.app.propertyeditors.MatiereEditor;
import com.app.propertyeditors.ModuleEditor;
import com.app.propertyeditors.NiveauEditor;
import com.app.propertyeditors.SemestreEditor;
import com.app.service.AnneeService;
import com.app.service.CentreService;
import com.app.service.FiliereService;
import com.app.service.FormationService;
import com.app.service.LogTrackerService;
import com.app.service.MatiereService;
import com.app.service.ModuleService;
import com.app.service.NiveauxService;
import com.app.service.SemestreService;

@Controller
public class MatiereNoteController {
	@Autowired
	ModuleService ModuleSvc;
	@Autowired
	CentreService centreSvc;
	@Autowired
	AnneeService anneeSvc;
	@Autowired
	NiveauxService niveauSvc;
	@Autowired
	FormationService formationSvc;
	@Autowired
	FiliereService filiereSvc;
	@Autowired
	SemestreService semestreSvc;
	@Autowired
	MatiereService matiereSvc;
	@Autowired
	LogTrackerService logSvc;
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Centre.class, new CentreEditor());
        binder.registerCustomEditor(Annee.class, new AnneeEditor());
        binder.registerCustomEditor(Niveaux.class, new NiveauEditor());
        binder.registerCustomEditor(Filiere.class, new FiliereEditor());
        binder.registerCustomEditor(Formation.class, new FormationEditor());
        binder.registerCustomEditor(Semestre.class, new SemestreEditor());
        binder.registerCustomEditor(Module.class, new ModuleEditor());
        binder.registerCustomEditor(Matiere.class, new MatiereEditor());
        
    }
	
	
	@PreAuthorize("hasRole('MODULE_READ')")
	@RequestMapping(value = "/notes/matieres", method = RequestMethod.GET)
	public String index(Model model , @ModelAttribute("moduleForm") Module moduleForm,
            BindingResult result ) 
	{
		//model.addAttribute("mode", "create");
		
		
		model.addAttribute("liste_centre", centreSvc.getAll());
	
		model.addAttribute("liste_annee", anneeSvc.getAll());
	
		model.addAttribute("liste_filiere", filiereSvc.getAll());
	
		model.addAttribute("liste_formation", formationSvc.getAll());
		
		model.addAttribute("liste_niveaux", niveauSvc.getAll());

		model.addAttribute("semestre", semestreSvc.getAll());
		
		model.addAttribute("liste_modules", ModuleSvc.getAll());
		
		model.addAttribute("liste_matieres", matiereSvc.getAll());
		return "/notes/matieres/liste";
	}
	
	

}
