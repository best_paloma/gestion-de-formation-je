package com.app.controller;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.enums.TypeVersement;
import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Etudiant;
import com.app.model.Filiere;
import com.app.model.Formation;
import com.app.model.Niveaux;
import com.app.model.Semestre;
import com.app.model.Versement;
import com.app.propertyeditors.AnneeEditor;
import com.app.propertyeditors.CentreEditor;
import com.app.propertyeditors.EtudiantEditor;
import com.app.propertyeditors.FiliereEditor;
import com.app.propertyeditors.FormationEditor;
import com.app.propertyeditors.NiveauEditor;
import com.app.propertyeditors.SemestreEditor;
import com.app.propertyeditors.TypeVersementEditor;
import com.app.service.AnneeService;
import com.app.service.CentreService;
import com.app.service.EtudiantService;
import com.app.service.FiliereService;
import com.app.service.FormationService;
import com.app.service.LogTrackerService;
import com.app.service.NiveauxService;
import com.app.service.SemestreService;
import com.app.service.VersementService;

@Controller
public class VersementController {
	@Autowired
	LogTrackerService logSvc;
	@Autowired
	AnneeService anneeService;
	@Autowired
	CentreService centreService;
	@Autowired
	FormationService formationService;
	@Autowired
	FiliereService filiereService;
	@Autowired
	NiveauxService niveauxService;
	@Autowired
	SemestreService semestreService;
	@Autowired
	EtudiantService etudiantService;
	@Autowired
	VersementService versementService;
	
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Etudiant.class, new EtudiantEditor());
        binder.registerCustomEditor(Niveaux.class, new NiveauEditor());
        binder.registerCustomEditor(Filiere.class, new FiliereEditor());
        binder.registerCustomEditor(Annee.class, new AnneeEditor());
        binder.registerCustomEditor(Centre.class, new CentreEditor());
        binder.registerCustomEditor(Formation.class, new FormationEditor());
        binder.registerCustomEditor(Semestre.class, new SemestreEditor());
        binder.registerCustomEditor(TypeVersement.class, new TypeVersementEditor());
    }
	
	@PreAuthorize("hasRole('ETUDIANT_LISTE_READ')")
	@RequestMapping(value = "/versement", method = RequestMethod.GET)
	public String index(Model model) 
	{
		model.addAttribute("liste_versements", versementService.getAll());
		return "versement/liste";
	}
	

	
	
	@PreAuthorize("hasRole('ETUDIANT_LISTE_EDIT')")
	@RequestMapping(value = "/versement/ajouter", method = RequestMethod.GET)
	public String ajouter(Model model) 
	{
		
		
		model.addAttribute("versementForm", new Versement());
		model.addAttribute("liste_versements", versementService.getAll());
		
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("formations", formationService.getAll());
		model.addAttribute("filieres", filiereService.getAll());
		model.addAttribute("niveaux", niveauxService.getAll());
		model.addAttribute("semestres", semestreService.getAll());
		model.addAttribute("etudiants", etudiantService.getAll());
		model.addAttribute("types_versement", TypeVersement.values());
		
		return "versement/ajouter";
	}
	
	@PreAuthorize("hasRole('ETUDIANT_LISTE_EDIT')")
	@RequestMapping(value = "/versement/save", method = RequestMethod.POST)
	public String save(
			@Valid @ModelAttribute("versementForm") Versement p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult.toString());
			
			model.addAttribute("versementForm", new Versement());
			
			model.addAttribute("liste_versements", versementService.getAll());
			model.addAttribute("annees", anneeService.getAll());
			model.addAttribute("centres", centreService.getAll());
			model.addAttribute("formations", formationService.getAll());
			model.addAttribute("filieres", filiereService.getAll());
			model.addAttribute("niveaux", niveauxService.getAll());
			model.addAttribute("semestres", semestreService.getAll());
			model.addAttribute("types_versement", TypeVersement.values());
			model.addAttribute("etudiants", etudiantService.getAll());

			
			return "versement/ajouter";
		} else {
			versementService.add(p);
			logSvc.store("Ajout versement: "+p, request);
			redirectAttributes.addFlashAttribute("success", "Le versement a ete bien ajouter");
			return "redirect:/versement/ajouter";

		}
	}
	
	@PreAuthorize("hasRole('ETUDIANT_LISTE_READ')")
	@RequestMapping(value = "/versement/edit", method = RequestMethod.GET)
	public String edit(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Versement p = versementService.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("versementForm", p);
		
		model.addAttribute("liste_versements", versementService.getAll());
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("formations", formationService.getAll());
		model.addAttribute("filieres", filiereService.getAll());
		model.addAttribute("niveaux", niveauxService.getAll());
		model.addAttribute("semestres", semestreService.getAll());
		model.addAttribute("types_versement", TypeVersement.values());
		model.addAttribute("etudiants", etudiantService.getAll());

		

		return "versement/modifier";
	}
	
	
	
	
	
	
	@PreAuthorize("hasRole('ETUDIANT_LISTE_EDIT')")
	@RequestMapping(value = "/versement/update", method = RequestMethod.POST)
	public String update(
			@Valid @ModelAttribute("versementForm") Versement p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			//System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("versementForm", p);

			model.addAttribute("liste_versements", versementService.getAll());
			model.addAttribute("annees", anneeService.getAll());
			model.addAttribute("centres", centreService.getAll());
			model.addAttribute("formations", formationService.getAll());
			model.addAttribute("filieres", filiereService.getAll());
			model.addAttribute("niveaux", niveauxService.getAll());
			model.addAttribute("semestres", semestreService.getAll());
			model.addAttribute("types_versement", TypeVersement.values());
			model.addAttribute("etudiants", etudiantService.getAll());


			return "versement/modifier";
		} else {
			
			try {
				versementService.update(p);
				logSvc.store("Mise a jour Versement : "+p, request);
				redirectAttributes.addFlashAttribute("success", "Le versement a ete bien modifier");
				return "redirect:/versement/edit?id="+p.getId();
			} catch (NotFoundException e) {
				redirectAttributes.addFlashAttribute("error", "Entity not found");
				return "redirect:/versement/edit?id="+p.getId();
			}
			
		}
	}
	
	
	
	
	
	
	@PreAuthorize("hasRole('ETUDIANT_LISTE_EDIT')")
	@RequestMapping(value = "/versement/delete", method = RequestMethod.GET)
	public String delete(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		
		try {
			Versement p = versementService.findById(id);
			versementService.delete(id);
			logSvc.store("Suppression Versement: "+p, request);
			redirectAttributes.addFlashAttribute("success_delete", "Le Versement  a ete bien supprimer");

			return "redirect:/versement";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_delete", "Entity not found");
			return "redirect:/versement";
		}
		
	}
	

}
