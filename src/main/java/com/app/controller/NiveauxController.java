package com.app.controller;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Filiere;
import com.app.model.Formation;
import com.app.model.Niveaux;
import com.app.propertyeditors.AnneeEditor;
import com.app.propertyeditors.CentreEditor;
import com.app.propertyeditors.FiliereEditor;
import com.app.propertyeditors.FormationEditor;
import com.app.service.AnneeService;
import com.app.service.CentreService;
import com.app.service.FiliereService;
import com.app.service.FormationService;
import com.app.service.LogTrackerService;
import com.app.service.NiveauxService;

@Controller
public class NiveauxController {
	@Autowired
	LogTrackerService logSvc;
	
	@Autowired
	FiliereService filiereSvc;
	@Autowired
	NiveauxService niveauxSvc;
	@Autowired
	AnneeService anneeService;
	@Autowired
	CentreService centreService;
	@Autowired
	FormationService formationService;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Annee.class, new AnneeEditor());
        binder.registerCustomEditor(Centre.class, new CentreEditor());
        binder.registerCustomEditor(Formation.class, new FormationEditor());
        binder.registerCustomEditor(Filiere.class, new FiliereEditor());
    }
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/niveaux", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("mode", "create");
		model.addAttribute("filiereForm", new Filiere());
		model.addAttribute("liste_filiere", filiereSvc.getAll());
		
		model.addAttribute("niveauxForm", new Niveaux());
		model.addAttribute("liste_niveaux", niveauxSvc.getAll());
		
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("formations", formationService.getAll());
		
		return "parametrage/niveaux";
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/saveFiliere", method = RequestMethod.POST)
	public String savePerson(
			@Valid @ModelAttribute("filiereForm") Filiere v,
			BindingResult bindingResult,
			@RequestParam(value = "mode", required = true, defaultValue = "create") String mode,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult.toString());
			model.addAttribute("filiereForm", v);
			model.addAttribute("liste_filiere", filiereSvc.getAll());
			
			model.addAttribute("niveauxForm", new Niveaux());
			model.addAttribute("liste_niveaux", niveauxSvc.getAll());
			
			model.addAttribute("centres", centreService.getAll());
			model.addAttribute("annees", anneeService.getAll());
			model.addAttribute("formations", formationService.getAll());
			return "parametrage/niveaux";
		} else {
			
			if(mode.equals("create"))
			{
				filiereSvc.add(v);
				logSvc.store("Ajout Filiere: "+v, request);
			
				redirectAttributes.addFlashAttribute("success_filiere", v.getIntitule() + " a ete bien ajouter");
				return "redirect:/parametrage/niveaux";
			}
			else{
				System.out.println("Updating "+v);
				try {
					filiereSvc.update(v);
					logSvc.store("Mise a jour Filiere: "+v, request);
					
					redirectAttributes.addFlashAttribute("success_filiere", v.getIntitule() + " a ete bien modifier");
					return "redirect:/parametrage/niveaux";
				} catch (NotFoundException e) {
					redirectAttributes.addFlashAttribute("error_filiere", "Entity not found");
					return "redirect:/parametrage/niveaux";
				}
				
			}
			
		}
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/saveNiveaux", method = RequestMethod.POST)
	public String saveNiveau(
			@Valid @ModelAttribute("niveauxForm") Niveaux v,
			BindingResult bindingResult,
			@RequestParam(value = "mode", required = true, defaultValue = "create") String mode,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			model.addAttribute("filiereForm", v);
			model.addAttribute("liste_filiere", filiereSvc.getAll());
			
			model.addAttribute("niveauxForm", new Niveaux());
			model.addAttribute("liste_niveaux", niveauxSvc.getAll());
			
			model.addAttribute("centres", centreService.getAll());
			model.addAttribute("annees", anneeService.getAll());
			model.addAttribute("formations", formationService.getAll());
			return "parametrage/niveaux";
		} else {
			
			
			if(mode.equals("create"))
			{
				niveauxSvc.add(v);
				logSvc.store("Ajout Niveau: "+v, request);
				
				redirectAttributes.addFlashAttribute("success_niveaux", v.getIntitule() + " a ete bien ajouter");
				return "redirect:/parametrage/niveaux";
			}
			else{
				System.out.println("Updating "+v);
				try {
					niveauxSvc.update(v);
					logSvc.store("Mise a jour Niveau: "+v, request);
					
					redirectAttributes.addFlashAttribute("success_niveaux", v.getIntitule() + " a ete bien modifier");
					return "redirect:/parametrage/niveaux";
				} catch (NotFoundException e) {
					redirectAttributes.addFlashAttribute("error_niveaux", "Entity not found");
					return "redirect:/parametrage/niveaux";
				}
				
			}
			
		}
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/editFiliere", method = RequestMethod.GET)
	public String editPerson(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Filiere p = filiereSvc.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("mode", "edit");
		model.addAttribute("filiereForm", p);
		model.addAttribute("niveauxForm", new Niveaux());
		
		
		model.addAttribute("liste_filiere", filiereSvc.getAll());
		model.addAttribute("liste_niveaux", niveauxSvc.getAll());
		
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("formations", formationService.getAll());
		return "parametrage/niveaux";
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/editNiveaux", method = RequestMethod.GET)
	public String editCentre(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Niveaux p = niveauxSvc.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("mode", "edit");
		model.addAttribute("niveauxForm", p);
		model.addAttribute("filiereForm", new Filiere());
		
		model.addAttribute("liste_filiere", filiereSvc.getAll());
		model.addAttribute("liste_niveaux", niveauxSvc.getAll());
		
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("formations", formationService.getAll());
		
		return "parametrage/niveaux";
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/deleteFiliere", method = RequestMethod.GET)
	public String deleteVille(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Filiere p = filiereSvc.findById(id);
			filiereSvc.delete(id);
			logSvc.store("Suppression Filiere: "+p, request);

			redirectAttributes.addFlashAttribute("success_filiere_delete", p.getIntitule()
					+ " a ete supprimer");

			return "redirect:/parametrage/niveaux";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_filiere_delete", "Entity not found");
			return "redirect:/parametrage/niveaux";
		}
		
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/deleteNiveaux", method = RequestMethod.GET)
	public String deleteCentre(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Niveaux p = niveauxSvc.findById(id);
			niveauxSvc.delete(id);
			logSvc.store("Suppression Niveau: "+p, request);

			redirectAttributes.addFlashAttribute("success_niveaux_delete", p.getIntitule()
					+ " a ete supprimer");

			return "redirect:/parametrage/niveaux";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_niveaux_delete", "Entity not found");
			return "redirect:/parametrage/niveaux";
		}
		
	}
	
}
