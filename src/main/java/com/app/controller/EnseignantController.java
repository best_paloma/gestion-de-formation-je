package com.app.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.Enseignant;
import com.app.model.Matiere;
import com.app.model.User;
import com.app.propertyeditors.UserEditor;
import com.app.service.EnseignantService;
import com.app.service.LogTrackerService;
import com.app.service.MatiereService;
import com.app.service.UserService;

@Controller
public class EnseignantController {
	@Autowired
	LogTrackerService logSvc;
	@Autowired
	EnseignantService enseignantService;
	@Autowired
	MatiereService matieretService;
	@Autowired
	UserService userService;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(User.class, new UserEditor());
    }
	
	@PreAuthorize("hasRole('ENSEIGNANT_READ')")
	@RequestMapping(value = "/employees/enseignant", method = RequestMethod.GET)
	public String index(Model model) 
	{
		model.addAttribute("liste", enseignantService.getAll());
		return "employees/enseignant/liste";
	}
	
	@PreAuthorize("hasRole('ENSEIGNANT_EDIT')")
	@RequestMapping(value = "/employees/enseignant/ajouter", method = RequestMethod.GET)
	public String ajouter(Model model) 
	{
	
		model.addAttribute("personnelForm", new Enseignant());
		Map<Integer,String> paiementList = new LinkedHashMap<Integer,String>();
		paiementList.put(1, "Chèque de paiement");
		paiementList.put(2, "Chèque visé");
		paiementList.put(3, "Chèque certifié");
		paiementList.put(4, "Chèque de banque");
		paiementList.put(5, "Espèce");
		paiementList.put(6, "Carte bancaire");
		model.addAttribute("paiementList", paiementList);
		
		Map<String,String> sexeList = new LinkedHashMap<String,String>();
		sexeList.put("Femme", "Femme");
		sexeList.put("Homme", "Homme");
		model.addAttribute("sexeList", sexeList);
		
		model.addAttribute("utilisateurs", userService.getAll()); // Change this to getAvailableUsers()
		
		return "employees/enseignant/ajouter";
	}
	
	@PreAuthorize("hasRole('ENSEIGNANT_EDIT')")
	@RequestMapping(value = "/employees/enseignant/savePersonnel", method = RequestMethod.POST)
	public String savePersonnel(
			@Valid @ModelAttribute("personnelForm") Enseignant p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("personnelForm", p);
			Map<Integer,String> paiementList = new LinkedHashMap<Integer,String>();
			paiementList.put(1, "Chèque de paiement");
			paiementList.put(2, "Chèque visé");
			paiementList.put(3, "Chèque certifié");
			paiementList.put(4, "Chèque de banque");
			paiementList.put(5, "Espèce");
			paiementList.put(6, "Carte bancaire");
			model.addAttribute("paiementList", paiementList);
			
			Map<String,String> sexeList = new LinkedHashMap<String,String>();
			sexeList.put("Femme", "Femme");
			sexeList.put("Homme", "Homme");
			model.addAttribute("sexeList", sexeList);
			model.addAttribute("utilisateurs", userService.getAll());
			
			
			return "employees/enseignant/ajouter";
		} else {
			
			enseignantService.add(p);
			logSvc.store("Ajout Enseignant: "+p, request);
			redirectAttributes.addFlashAttribute("success_personnel", p.getPrenom() + " a ete bien ajouter");
			return "redirect:/employees/enseignant/ajouter";

		}
	}
	
	@RequestMapping(value = "/employees/enseignant/editPerson", method = RequestMethod.GET)
	public String editPerson(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) throws NotFoundException {
		Enseignant p = enseignantService.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("personnelForm", p);
		Map<Integer,String> paiementList = new LinkedHashMap<Integer,String>();
		paiementList.put(1, "Chèque de paiement");
		paiementList.put(2, "Chèque visé");
		paiementList.put(3, "Chèque certifié");
		paiementList.put(4, "Chèque de banque");
		paiementList.put(5, "Espèce");
		paiementList.put(6, "Carte bancaire");
		model.addAttribute("paiementList", paiementList);
		
		Map<String,String> sexeList = new LinkedHashMap<String,String>();
		sexeList.put("Femme", "Femme");
		sexeList.put("Homme", "Homme");
		model.addAttribute("sexeList", sexeList);
		model.addAttribute("utilisateurs", userService.getAll());

		List<Matiere> liste = matieretService.findByMatiere(p);

		model.addAttribute("liste_matieres", liste);

		return "employees/enseignant/modifier";
	}
	
	@PreAuthorize("hasRole('ENSEIGNANT_EDIT')")
	@RequestMapping(value = "/employees/enseignant/updatePersonnel", method = RequestMethod.POST)
	public String updatePersonnel(
			@Valid @ModelAttribute("personnelForm") Enseignant p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			//System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("personnelForm", p);
			Map<Integer,String> paiementList = new LinkedHashMap<Integer,String>();
			paiementList.put(1, "Chèque de paiement");
			paiementList.put(2, "Chèque visé");
			paiementList.put(3, "Chèque certifié");
			paiementList.put(4, "Chèque de banque");
			paiementList.put(5, "Espèce");
			paiementList.put(6, "Carte bancaire");
			model.addAttribute("paiementList", paiementList);
			
			Map<String,String> sexeList = new LinkedHashMap<String,String>();
			sexeList.put("Femme", "Femme");
			sexeList.put("Homme", "Homme");
			model.addAttribute("sexeList", sexeList);
			
			model.addAttribute("utilisateurs", userService.getAll());
			
			return "employees/enseignant/modifier";
		} else {
			
			try {
				enseignantService.update(p);
				logSvc.store("Mise a jour Enseignant: "+p, request);
				redirectAttributes.addFlashAttribute("success_personnel", p.getPrenom() + " a ete bien modifier");
				return "redirect:/employees/enseignant/editPerson?id="+p.getId();
			} catch (NotFoundException e) {
				redirectAttributes.addFlashAttribute("error_personnel", "Entity not found");
				return "redirect:/employees/enseignant/editPerson?id="+p.getId();
			}
			
		}
	}
	
	@PreAuthorize("hasRole('ENSEIGNANT_EDIT')")
	@RequestMapping(value = "/employees/enseignant/deletePerson", method = RequestMethod.GET)
	public String deleteVille(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Enseignant p = enseignantService.findById(id);
			enseignantService.delete(id);
			logSvc.store("Suppression enseignant: "+p, request);

			redirectAttributes.addFlashAttribute("success_person_delete", p.getPrenom()
					+ " a ete supprimer");

			return "redirect:/employees/enseignant";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_person_delete", "Entity not found");
			return "redirect:/employees/enseignant";
		}
		
	}
	
	
	
	
	
	
	
	@PreAuthorize("hasRole('ENSEIGNANT_EDIT')")
	@RequestMapping(value = "/employees/enseignant/listeMatiere", method = RequestMethod.GET)
	public String listeMatiere(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Enseignant ens = new Enseignant();
			ens = enseignantService.findById(id);
			List<Matiere> liste = matieretService.findByMatiere(ens);
			
			System.out.println("********************************** LIST :  *******************************"+liste);
			System.out.println("******************************** **********************"+ens);

			

			return "redirect:/employees/enseignant";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_person_delete", "Entity not found");
			return "redirect:/employees/enseignant";
		}
		
	}
	
	
	
	

}
