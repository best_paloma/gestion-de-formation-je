package com.app.controller;

import java.util.ArrayList;
import java.util.List;

import javassist.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.json.JResponse;
import com.app.model.Message;
import com.app.model.User;
import com.app.service.MessageService;
import com.app.service.UserService;

@Controller
public class AjaxController {
	@Autowired
	MessageService messageService;
	@Autowired
	UserService userService;
	
	
	@RequestMapping(value = "/data/get_total_messages", method = RequestMethod.GET)
	public @ResponseBody JResponse getTotalMessages() {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
		try {
			User u = userService.findByUsername(userDetails.getUsername());
			return new JResponse("200",""+messageService.count(u));
		} catch (NotFoundException e) {
			return new JResponse("504", "user not found");
		}
		

	}
	
	@RequestMapping(value = "/data/get_new_messages", method = RequestMethod.GET)
	public @ResponseBody List<Message> getNewMessages() {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
		try {
			User u = userService.findByUsername(userDetails.getUsername());
			return messageService.getLast(u);
		} catch (NotFoundException e) {
			List<Message> m = new ArrayList<Message>();
			return m;
		}
		

	}
	
}
