package com.app.controller;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.Annee;
import com.app.service.AnneeService;
import com.app.service.LogTrackerService;


@Controller
public class AnneeController {
	@Autowired
	AnneeService AnneeSvc;
	@Autowired
	LogTrackerService logSvc;


	
	@RequestMapping(value = "/parametrage/annee", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("mode", "create");
		
		Annee a = new Annee();
		a.setActived(false);
		model.addAttribute("anneeForm", a);
		model.addAttribute("liste_annees", AnneeSvc.getAll());
		
		return "/parametrage/annee/liste";
	}
	
	
	@RequestMapping(value = "/parametrage/annee/ajouter", method = RequestMethod.GET)
	public String ajouter(Model model) 
	{
		model.addAttribute("anneeForm", new Annee());
		model.addAttribute("liste_annees", AnneeSvc.getAll());
		return "parametrage/annee/ajouter";
	}
	
	@RequestMapping(value = "/parametrage/annee/saveAnnee", method = RequestMethod.POST)
	public String saveAnnee(
			@Valid @ModelAttribute("anneeForm") Annee p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			//System.out.println("Erreur");
			//System.out.println(bindingResult.toString());
			model.addAttribute("anneeForm", p);
			model.addAttribute("liste_annees", AnneeSvc.getAll());
			
			return "parametrage/annee/ajouter";
		} else {
			
			AnneeSvc.add(p);
			logSvc.store("Ajout Role: "+p, request);
			redirectAttributes.addFlashAttribute("success_annee", p.getIntitule() + " a ete bien ajouter");
			return "redirect:/parametrage/annee/ajouter";

		}
	}
	
	
	@RequestMapping(value = "/parametrage/annee/edit", method = RequestMethod.GET)
	public String edit(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Annee p = AnneeSvc.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("anneeForm", p);
		model.addAttribute("liste_annees", AnneeSvc.getAll());
		
		return "parametrage/annee/modifier";
	}
	
	
	@RequestMapping(value = "/parametrage/annee/update", method = RequestMethod.POST)
	public String update(
			@Valid @ModelAttribute("anneeForm") Annee p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			model.addAttribute("anneeForm", p);
			model.addAttribute("liste_annees", AnneeSvc.getAll());
			
			return "parametrage/annee/modifier";
		} else {
			
			try {
				AnneeSvc.update(p);
				logSvc.store("Mise a jour Role: "+p, request);
				redirectAttributes.addFlashAttribute("success_annee", p.getIntitule() + " a ete bien modifier");
				return "redirect:/parametrage/annee/edit?id="+p.getId();
			} catch (NotFoundException e) {
				redirectAttributes.addFlashAttribute("error_annee", "Entity not found");
				return "redirect:/parametrage/annee/edit?id="+p.getId();
			}
			


		}
	}
	
	
	
	
	@RequestMapping(value = "/parametrage/annee/delete", method = RequestMethod.GET)
	public String delete(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Annee p = AnneeSvc.findById(id);
			AnneeSvc.delete(id);
			logSvc.store("Suppression Annee : "+p, request);

			redirectAttributes.addFlashAttribute("success_annee_delete", p.getIntitule()
					+ " a ete supprimer");

			return "redirect:/parametrage/annee";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_annee_delete", "Entity not found");
			return "redirect:/parametrage/annee";
		}
		
	}
	
}
	
	
	
	