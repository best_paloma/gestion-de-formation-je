package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.model.User;

public class UserEditor extends PropertyEditorSupport {
	
	// Converts a String to a User (when submitting form)
    @Override
    public void setAsText(String text) {
    	int id=Integer.valueOf(text);
    	User c = new User();
    	c.setId(id);
        this.setValue(c);
    }

    // Converts a User to a String (when displaying form)
    @Override
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	User c = (User) this.getValue();
    	if (c != null)
            return c.getUsername();
        return null;
    }

}
