package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.model.Emploie_Temps;



public class EmploieEditor extends PropertyEditorSupport {
	
	// Converts a String to a Permission (when submitting form)
    @Override
    public void setAsText(String text) {
    	int id=Integer.valueOf(text);
    	Emploie_Temps c = new Emploie_Temps();
    	c.setId(id);
        this.setValue(c);
    }

    // Converts a Permission to a String (when displaying form)
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	Emploie_Temps c = (Emploie_Temps) this.getValue();
    	if (c != null)
            return c.getIntitule();
        return null;
    }

}
