package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Emploie_Temps;

public class EmploieService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Emploie_Temps> getAll() {
		List<Emploie_Temps> result = em.createQuery("SELECT p FROM Emploie_Temps p",
				Emploie_Temps.class).getResultList();
		return result;
	}

	@Transactional
	public Emploie_Temps add(Emploie_Temps p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		return p;


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Emploie_Temps p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Emploie_Temps update(Emploie_Temps p) throws NotFoundException{
		Emploie_Temps up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Emploie_Temps findById(int id) {
		return em.find(Emploie_Temps.class, id);
	}
}