package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Annee;
import com.app.model.Controle;
import com.app.model.Module;

public class ControleService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Controle> getAll() {
		List<Controle> result = em.createQuery("SELECT p FROM Controle p",
				Controle.class).getResultList();
		return result;
	}
	@Transactional
	public List<Controle> findbyYear(Annee annee) {
		List<Controle> result = em.createQuery("SELECT p FROM Controle p where annee= :annee ",
				Controle.class).setParameter("annee", annee).getResultList();
		return result;
	}

	@Transactional
	public void add(Controle p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Controle p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Controle update(Controle p) throws NotFoundException{
		Controle up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Controle findById(int id) {
		return em.find(Controle.class, id);
	}
}