package com.app.service;

import java.util.List;
import javassist.NotFoundException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import com.app.model.Ville;

public class VilleService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Ville> getAll() {
		List<Ville> result = em.createQuery("SELECT p FROM Ville p",
				Ville.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Ville p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Ville p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Ville update(Ville p) throws NotFoundException{
		Ville up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Ville findById(int id) {
		return em.find(Ville.class, id);
	}
}