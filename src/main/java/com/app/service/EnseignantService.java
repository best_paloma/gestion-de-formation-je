package com.app.service;

//import java.sql.Date;
import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Enseignant;

public class EnseignantService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Enseignant> getAll() {
		List<Enseignant> result = em.createQuery("SELECT p FROM Enseignant p",
				Enseignant.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Enseignant p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Enseignant p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Enseignant update(Enseignant p) throws NotFoundException{
		Enseignant up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Enseignant findById(int id) {
		return em.find(Enseignant.class, id);
	}
}