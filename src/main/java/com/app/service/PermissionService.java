package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Permission;

//@Service
public class PermissionService {
	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public List<Permission> getAll() {
		List<Permission> result = em.createQuery("SELECT p FROM Permission p",
				Permission.class).getResultList();
		return result;
	}

	
	@Transactional
	public void add(Permission p) {
		em.persist(p);
	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Permission p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Permission update(Permission p) throws NotFoundException{
		Permission up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Permission findById(int id) {
		return em.find(Permission.class, id);
	}
	
	@Transactional
	public Permission findByName(String name) {
		List<Permission> result = em.createQuery("SELECT p FROM Permission p WHERE name = :name",
				Permission.class).setParameter("name", name).getResultList();
	    
	    return result.get(0);
	}

}
