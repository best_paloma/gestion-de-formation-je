package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.AffecterMatiere;
public class AffecterMatiereService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<AffecterMatiere> getAll() {
		List<AffecterMatiere> result = em.createQuery("SELECT p FROM AffecterMatiere p",
				AffecterMatiere.class).getResultList();
		return result;
	}
	
	@Transactional
	public void add(AffecterMatiere p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		AffecterMatiere p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public AffecterMatiere update(AffecterMatiere p) throws NotFoundException{
		AffecterMatiere up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public AffecterMatiere findById(int id) {
		return em.find(AffecterMatiere.class, id);
	}
}