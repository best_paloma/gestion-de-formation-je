package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.enums.AbsenceType;
import com.app.model.Absence;


public class AbsenceService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Absence> getAll() {
		List<Absence> result = em.createQuery("SELECT p FROM Absence p",
				Absence.class).getResultList();
		return result;
	}
	
	@Transactional
	public List<Absence> getAllByType(AbsenceType absence) {
		List<Absence> result = em.createQuery("SELECT p FROM Absence p WHERE type = :type",
				Absence.class).setParameter("type", absence).getResultList();
		return result;
	}
	
	@Transactional
	public int totalByMonthYear(String month, String year, int absence) {
		Object result = em.createNativeQuery("SELECT count(p.id) FROM Absence p where p.type = :type and month(p.date_seance)=:month and year(p.date_seance)=:year")
				.setParameter("type", absence)
				.setParameter("month", month)
				.setParameter("year", year)
				.getSingleResult();
		em.flush();
		
		return Integer.parseInt(result.toString());
	}

	@Transactional
	public void add(Absence p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Absence p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Absence update(Absence p) throws NotFoundException{
		Absence up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Absence findById(int id) {
		return em.find(Absence.class, id);
	}
}