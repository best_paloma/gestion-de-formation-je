package com.app.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.InheritanceType;
import javax.persistence.DiscriminatorType;

@Entity
@Table(name = "personnel")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn( name="personneltype", discriminatorType=DiscriminatorType.STRING      )
public class Person {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_person")
	private int id;
	
	@ManyToOne
    @JoinTable(name = "person_utilisateur", joinColumns = @JoinColumn(name = "id_person"), inverseJoinColumns = @JoinColumn(name = "id_user"))
	private User utilisateur;

	@NotEmpty(message = "Entrez un prénom.")
	@Size(min = 4, max = 20, message = "Le prénom doit être compris entre 4 et 20 caractères")
	private String prenom;

	@NotEmpty(message = "Entrez un nom.")
	@Size(min = 4, max = 20, message = "Le nom doit être compris entre 4 et 20 caractères")
	private String nom;
	
	
	private String sexe;
	private String cin;
	private String telephone;
	private String email;
	@Column
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private DateTime date_entree;
	@Column
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private DateTime date_sortie;
	
	private String banque;
	private String rib;
	private int type_paiement;
	private String fullname;
	

	public String getFullname() {
		return nom +" "+ prenom;
	}



	public void setFullname(String fullname) {
		this.fullname = fullname;
	}



	public Person() {
	}



	public Person(int id, String prenom, String nom,
			String sexe, String cin, String telephone, String email,
			DateTime date_entree, DateTime date_sortie, String banque,
			String rib, int type_paiement) {
		super();
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.sexe = sexe;
		this.cin = cin;
		this.telephone = telephone;
		this.email = email;
		this.date_entree = date_entree;
		this.date_sortie = date_sortie;
		this.banque = banque;
		this.rib = rib;
		this.type_paiement = type_paiement;
	}




	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getPrenom() {
		return prenom;
	}



	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getSexe() {
		return sexe;
	}



	public void setSexe(String sexe) {
		this.sexe = sexe;
	}



	public String getCin() {
		return cin;
	}



	public void setCin(String cin) {
		this.cin = cin;
	}



	public String getTelephone() {
		return telephone;
	}



	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public DateTime getDate_entree() {
		return date_entree;
	}



	public void setDate_entree(DateTime date_entree) {
		this.date_entree = date_entree;
	}



	public DateTime getDate_sortie() {
		return date_sortie;
	}



	public void setDate_sortie(DateTime date_sortie) {
		this.date_sortie = date_sortie;
	}



	public String getBanque() {
		return banque;
	}



	public void setBanque(String banque) {
		this.banque = banque;
	}



	public String getRib() {
		return rib;
	}



	public void setRib(String rib) {
		this.rib = rib;
	}



	public int getType_paiement() {
		return type_paiement;
	}



	public void setType_paiement(int type_paiement) {
		this.type_paiement = type_paiement;
	}

	public User getUtilisateur() {
		return utilisateur;
	}



	public void setUtilisateur(User utilisateur) {
		this.utilisateur = utilisateur;
	}



	@Override
	public String toString() {
		return nom;
	}
	
	

	
}